// import ImageSlide from '../components/ImageSlide';
import Home from './Home';
import PhoneRegister from './PhoneRegister';
import Chat from './Chat';
import Main from './Main';
import PlayAccess from '../components/PlayAccess';
import Transaction from '../components/Transaction';
import ChangePassword from '../components/ChangePassword';
import Promotion from './Promotion';
import Announce from './Announce';


export {
    Home,
    PhoneRegister,
    Chat,
    Main,
    PlayAccess,
    Transaction,
    ChangePassword,
    Promotion,
    Announce
}