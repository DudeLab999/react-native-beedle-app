import React, { Component } from 'react';
import { CheckBox, Alert, View, Button, Text, StyleSheet, TextInput, Image, TouchableOpacity, Dimensions } from "react-native";
import SignupForm from '../components/SignupForm';
import { WebView } from 'react-native-webview';

import axios from 'axios';

require('../global.js');

const registerToken = global.registerToken;
const apiUrl = global.API_URL;

const otpTimeout = 30000 // Seconds
const resentOtpButtonInterval = 30 // Seconds

const BANK_ICON = {
    "SCB ธนาคารไทยพาณิชย์": require('../assets/img/bank_icons/scb.jpeg'),
    "KBANK ธนาคารกสิกรไทย": require('../assets/img/bank_icons/kbank.png'),
    "KTB ธนาคารกรุงไทย": require('../assets/img/bank_icons/ktb.png'),
    "BBL ธนาคารกรุงเทพ": require('../assets/img/bank_icons/bbl.jpg'),
    "BAY ธนาคารกรุงศรี": require('../assets/img/bank_icons/bay.png'),
    "TBANK ธนาคารธนชาต": require('../assets/img/bank_icons/tbank.png'),
    "TMB ธนาคารทหารไทย": require('../assets/img/bank_icons/tmb.png'),
    "GSB ธนาคารออมสิน": require('../assets/img/bank_icons/gsb.jpg'),
    "BAG ธกส.": require('../assets/img/bank_icons/bag.png'),
    "UOB ธนาคารยูโอบี": require('../assets/img/bank_icons/uob.png'),
    "CIMB ธนาคารซีไอเอ็มบี": require('../assets/img/bank_icons/cimb.png'),
    "TTTB ธนาคารทหารไทยธนชาต": require('../assets/img/bank_icons/tttb.png'),
}

class PhoneRegister extends Component {

    constructor(props) {
        super(props)
        const dimensions = Dimensions.get('window');

        this.bankDropdownRef = React.createRef();

        let inviteUser = "";

        try {
            if (props.route.params.id != null && props.route.params.id != undefined) {
                inviteUser = props.route.params.id;
            }
        } catch (err) {

        }

        this.state = {
            inviteUser: inviteUser,
            lineHtml: "",
            selectTerms: false,
            showCondition: true,
            showLineLogin: false,
            showRequestOtp: false,
            showConfirmOtp: false,
            showRegisterForm: false,
            showRegisterAcknowledge: false,

            windowWidth: dimensions.width,
            windowHeight: dimensions.height,
            showDropdown: false,

            bankId: null,
            bankName: '',
            bankIconSource: null,
            phoneNumberOtp: null,

            showNewRequestOtpButton: false,
            resentOtpPressed: false,

            otpRef: null,
            otpNumber: null,
            otpCorrect: false,
            otpExpiredTimeout: 0,
            otpExpired: false,

            otpCountLeft: 4,
            otpTimeCount: resentOtpButtonInterval,
            otpInterval: 0,

            otpPressed: false,
            otpInvalid: null,

            registerForm: {
                firstname: "",
                lastname: "",
                nickname: "",
                bankNo: "",
                bankName: "",
                lineId: "",
            }
        }
    }

    submitForm = (values) => {
        this.setState(prevState => ({
            registerForm: {
                firstname: values.firstname,
                lastname: values.lastname,
                nickname: values.nickname,
                bankNo: values.bankNo,
                bankName: prevState.bankName,
                lineId: values.lineID,
                email: values.email,
                sid: values.sid,
                phoneNumber: values.phoneno
            }
        }))

        try {
            const config = {
                headers: { Authorization: '' }
            };

            axios.post
                (
                    `${apiUrl}/registerMobile`,
                    {
                        'invitekey': this.state.inviteUser,
                        "phone_number": values.phoneno,
                        "line": values.lineID,
                        "bank": this.state.bankId,
                        "bank_account": values.bankNo,
                        "nickname": values.nickname,
                        "firstname": values.firstname,
                        "lastname": values.lastname,
                        "email": values.email,
                        "sid": values.sid
                        // "pin": this.state.otpNumber,
                        // "ref": this.state.otpRef,
                    },
                    config
                )
                .then((response) => {
                    try {
                        if (response.status == 200) {
                            console.log(response)

                            const result = response.data;
                            if (result.status) {
                                this.setState(({
                                    lineUrl: result.line,
                                    // otpExpiredTimeOut: this.otpExpiredTimeOut()
                                }))

                                this.hideOtpSubmit();

                            } else {
                                if (result.error == 2) {
                                    Alert.alert(result.message);
                                    this.setState(({
                                        showRegisterForm: true,
                                    }))
                                } else {
                                    Alert.alert("Error occur", "Please try again");
                                }
                            }
                            // this.setState({ loading: false, lineContact: newUrl })
                        }
                    } catch (e) {
                        console.log(e)
                        Alert.alert("Error occur", "Please try again");
                        this.props.navigation.reset({ index: 0, routes: [{ name: 'Home' }], });
                    }

                })
                .catch((error) => {
                    console.log(error)
                    this.setState({ loading: false })

                    if (error.response) {
                        Alert.alert("Error occur", "Please try again");
                        if (error.response.status == 401) {

                        }
                        this.props.navigation.reset({ index: 0, routes: [{ name: 'Home' }], });
                    }

                });
        } catch (err) {

        }

    }

    otpRequest = (phoneNumber) => {
        try {
            const config = {
                headers: { Authorization: '' }
            };

            axios.post
                (
                    `${apiUrl}/requestOtp`,
                    {
                        "phone": phoneNumber
                    },
                    config
                )
                .then((response) => {
                    if (response.status == 200) {
                        const result = response.data;

                        if (result.error == 2) {
                            this.setState(({
                                showRequestOtp: true,
                                showConfirmOtp: false,
                                showRegisterForm: false,
                            }))
                        }

                        if (result.status) {
                            console.log("From server: " + result.otp);
                            console.log("REF: " + result.ref);

                            this.setState(({
                                phoneNumber: phoneNumber,
                                otpRef: result.ref,
                                otpNumber: result.otp,
                                otpExpiredTimeOut: this.otpExpiredTimeOut()
                            }))

                            // alert(result.message);
                            return result.otp

                        } else {
                            Alert.alert(result.message);
                            return "";
                        }
                        // this.setState({ loading: false, lineContact: newUrl })
                    }
                })
                .catch((error) => {
                    console.log(error)
                    this.setState({ loading: false })

                    if (error.response) {
                        if (error.response.status == 401) {

                        }
                        this.props.navigation.reset({ index: 0, routes: [{ name: 'Home' }], });
                    }

                    return "";
                });
        } catch (error) {
            Alert.alert("Error occur", "Please try again");
            console.error(error)
            return "";
        }
    }

    resentOtpRequest = async () => {
        await this.otpRequest(this.state.phoneNumberOtp)

        clearTimeout(this.state.otpExpiredTimeOut)

        this.setState(prevState => {
            if (prevState.otpExpired) {
                return {
                    resentOtpPressed: false,
                    otpExpired: false,
                }
            }
            return { resentOtpPressed: false }
        }
        )
    }

    updatePhoneNumberOTP = (phoneNumber) => {
        this.setState(({
            phoneNumberOtp: phoneNumber
        }))
        this.hideOtpRequest()
        this.otpRequest(phoneNumber);
    }

    hideOtpRequest = () => {
        this.setState(prevState => ({
            showRequestOtp: !prevState.showRequestOtp,
            showConfirmOtp: true,
        }))
    }

    verifyOtp = () => {
        this.setState(prevState => ({
            otpPressed: true,
            otpInvalid: true,
            otpCountLeft: prevState.otpCountLeft - 1

        }))

        if (this.state.otpCountLeft <= 1) {
            this.setState(({
                otpCountLeft: 0,
                otpPressed: false,

                otpInterval: setInterval(() => {
                    this.setState(prevState => ({
                        otpTimeCount: prevState.otpTimeCount - 1
                    }))

                    if (this.state.otpTimeCount == 0) {
                        clearInterval(this.state.otpInterval)

                        this.setState(({
                            showNewRequestOtpButton: true,
                            otpTimeCount: resentOtpButtonInterval,
                            otpCountLeft: 4,
                            resentOtpPressed: false,
                        }))
                    }
                }, 1000)
            }))

        }

        if (this.state.otpCorrect) {
            this.setState(({
                otpInvalid: false,
            }))

            clearInterval(this.state.otpInterval)
            clearTimeout(this.state.otpExpiredTimeOut)

            // Check server again
            const config = {
                headers: { Authorization: '' }
            };
            axios.post
                (
                    `${apiUrl}/checkOtp`,
                    {
                        'phone': this.state.phoneNumber,
                        'txtRef': this.state.otpRef,
                        'pin': this.state.otpNumber
                    },
                    config
                )
                .then((response) => {
                    if (response.status == 200) {
                        const result = response.data;
                        console.log(result)
                        if (result.status) {
                            this.hideConfirmOtp();
                        } else {
                            Alert.alert(result.message)
                        }

                    }
                })
                .catch((error) => {
                    console.log(error)
                    this.setState({ loading: false })

                    if (error.response) {

                        this.props.navigation.reset({ index: 0, routes: [{ name: 'Home' }], });
                    }
                });


        }
    }

    checkOtpOnType = (otpNumber) => {
        if (otpNumber == this.state.otpNumber) {
            this.setState(({
                otpCorrect: true
            }))
        } else {
            this.setState(({
                otpCorrect: false
            }))
        }

        // console.log(otpNumber + " : " + this.state.otpNumber)
    }

    otpExpiredTimeOut = () => (
        setTimeout(() => {
            this.setState(({
                otpNumber: null,
                otpRef: null,
                otpExpired: true,
                showNewRequestOtpButton: true,
            }))
            console.log("OTP Expired!")
        }, otpTimeout))

    hideConfirmOtp = () => {
        this.setState(prevState => ({
            showConfirmOtp: !prevState.showConfirmOtp,
            showRegisterForm: true,
        }))
    }

    hideOtpSubmit = () => {
        // console.log("hideOtpSubmit....")
        this.setState(prevState => ({
            showRegisterForm: !prevState,
            showRegisterAcknowledge: true,
        }))
    }

    _adjustFrame = (style) => {
        style.width = (this.state.windowWidth) * 0.9;
        style.top = (this.state.windowHeight) * 0.2;
        // style.left += 100;
        style.height = (this.state.windowHeight) * 0.7;
        return style;
    }

    _dropdownRenderRow = (rowData, rowID, highlighted) => {
        let icon = BANK_ICON[rowData];
        return (
            <TouchableOpacity onPress={() => this._onSelect(rowData, rowID, highlighted, icon)}>
                <View style={phoneRegisterStyle.bankDropdown}>
                    <Image style={phoneRegisterStyle.dropdownImage}
                        mode='contain'
                        source={icon}
                    />
                    <Text style={phoneRegisterStyle.dropdownText}>
                        {`${rowData}`}
                    </Text>
                </View>
            </TouchableOpacity>
        );
    }

    _onSelect = (rowData, rowID, highlighted, icon) => {
        this.setState((prevState) => ({
            bankId: rowID,
            bankName: rowData,
            bankIconSource: icon,
        }))
        // console.log(this.bankDropdownRef)

        this.bankDropdownRef && this.bankDropdownRef.current.hide();
    }

    _dropdownRenderSeparator(sectionID, rowID, adjacentRowHighlighted) {
        if (rowID == OPTIONS.length - 1) return;
        let key = `spr_${rowID}`;
        return (<View style={{ height: 1, backgroundColor: 'grey' }}
            key={key}
        />);
    }

    _dropdownRenderButtonText(rowData) {
        return rowData;
    }

    _dropdownOnSelect(idx, value) {
        console.log(idx, value)
        this._dropdown_idx = idx;
        if (this._dropdown_idx != 0) {
            return false;
        }
    }

    componentWillUnmount() {

        clearInterval(this.state.otpInterval)
        clearTimeout(this.state.otpExpiredTimeOut)

    }

    selectTerm = () => {
        this.setState((prevState) => ({
            selectTerms: !prevState.selectTerms,
        }))
    }

    showRegister = () => {
        this.setState((prevState) => ({
            showCondition: false,
            showLineLogin: true,
        }))
    }

    randomString = () => {
        let randomChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()";
        let result = "";
        for (let i = 0; i < 8; i++) {
            result += randomChars.charAt(Math.floor(Math.random() * randomChars.length));
        }
    }

    onMessage = (event) => {
        try {
            let data = event.nativeEvent.data;
            data = JSON.parse(data);

            if (data.params.ref != undefined && data.params.ref != "" && data.params.result) {
                console.log(data);
                this.setState({
                    email: data.params.email,
                    sid: data.params.sid,
                    authLineCode: data.params.ref,
                    authLineState: data.params.result,
                    showLineLogin: false,
                    showRegisterForm: true
                });
            } else {
                console.log("--email exist--");
                console.log(data);
                this.setState({
                    showLineLogin: false,
                    showFailMessage: true
                });
            }
        } catch (error) {
            console.log(event)
        }
    }

    componentDidMount() {

    }

    render() {
        if (this.state.showRequestOtp) {
            return (
                <SignupForm type={"otp"} updatePhoneNumberOTP={this.updatePhoneNumberOTP} />
            )
        } else if (this.state.showConfirmOtp) {
            return (
                <SignupForm
                    type={"confirmOTP"}
                    phoneNumberOtp={this.state.phoneNumberOtp}
                    otpRef={this.state.otpRef}
                    otpPressed={this.state.otpPressed}
                    otpInvalid={this.state.otpInvalid}
                    otpCountLeft={this.state.otpCountLeft}
                    otpCountLeft={this.state.otpCountLeft}
                    otpTimeCount={this.state.otpTimeCount}
                    otpTimeCount={this.state.otpTimeCount}
                    otpExpired={this.state.otpExpired}
                    showNewRequestOtpButton={this.state.showNewRequestOtpButton}
                    otpExpired={this.state.otpExpired}
                    resentOtpPressed={this.state.resentOtpPressed}
                    resentOtpPressed={this.state.resentOtpPressed}
                    otpCountLeft={this.state.otpCountLeft}
                    otpCountLeft={this.state.otpCountLeft}
                    checkOtpOnType={this.checkOtpOnType}
                    resentOtpRequest={this.resentOtpRequest}
                    verifyOtp={this.verifyOtp}
                />
            )
        }
        else if (this.state.showRegisterForm) {
            return (
                <SignupForm type={"signup"}
                    ref={this.bankDropdownRef}
                    _adjustFrame={this._adjustFrame}
                    _dropdownRenderRow={this._dropdownRenderRow}
                    bankName={this.state.bankName}
                    email={this.state.email}
                    sid={this.state.sid}
                    bankIconSource={this.state.bankIconSource}
                    hideOtpSubmit={this.hideOtpSubmit}
                    submitForm={this.submitForm}
                />
            )
        }
        else if (this.state.showRegisterAcknowledge) {
            return (
                <SignupForm type={"acknowledge"} lineUrl={this.state.lineUrl} />
            )
        }
        else if (this.state.showCondition) {
            return (
                <View style={{ flex: 1, marginTop: '50%', backgroundColor: "white" }}>
                    <Text style={{ marginTop: 30, color: "black", alignSelf: "center" }}>
                        Terms & conditions
                    </Text>
                    <View style={{ flexDirection: "row" }}>
                        <CheckBox
                            value={this.state.selectTerms}
                            onValueChange={() => this.selectTerm()}
                            style={{ flexDirection: "row", alignSelf: "center", marginBottom: 20, }}
                        />
                        <Text style={{ color: "black", alignSelf: "center", flexDirection: "row", alignSelf: "center" }}>
                            I accept and agree to provide the user's {"\n"} public information (user profile and email) to register.
                        </Text>
                    </View>

                    <Text style={{ marginTop: 30, color: "black", alignSelf: "center", textDecorationLine: 'underline' }}>
                        กรุณายอมรับเงื่อนไขในการยืนยันตัวตน ผ่าน Line
                    </Text>

                    <TouchableOpacity style={{ marginTop: '15%', alignSelf: 'center', width: '100%' }}
                        onPress={() => this.showRegister()}
                        disabled={this.state.selectTerms ? false : true}
                    >
                        <Image
                            resizeMode='contain'
                            source={this.state.selectTerms ? require("../assets/base_line_login.png") : require("../assets/btn_login_disable.png")}
                            style={{ width: '50%', height: '30%', alignSelf: 'center' }}
                        />
                    </TouchableOpacity>
                </View>
            )
        }
        else if (this.state.showLineLogin) {
            const randomNounce = this.randomString();
            return (
                <View style={{ flex: 1, marginTop: '40%' }}>
                    <WebView
                        ref="lineWebView"
                        onMessage={(m) => this.onMessage(m)}
                        source={{
                            uri: `https://google.com`
                        }}
                        style={{ marginTop: 20 }}
                    />
                </View>

            )
        }
        else if (this.state.showFailMessage) {
            return (
                <View style={{ marginTop: '50%', backgroundColor: "white", height: '15%' }}>
                    <Text style={{ marginTop: 30, color: "black", alignSelf: "center" }}>
                        Can't register
                    </Text>
                    <Text style={{ color: "black", alignSelf: "center" }}>Email exist</Text>
                </View>

            )
        }
    }

}

export default PhoneRegister;

const phoneRegisterStyle = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 20,
        alignItems: 'center',
        flexDirection: 'column',
        // borderWidth: 1
    },
    title: {
        margin: 12,
        alignSelf: 'flex-start',
        fontSize: 15,
        color: 'white',

    },
    bankDropdown: {
        margin: 12,
        flexDirection: 'row',
        height: 50,
        justifyContent: 'flex-start',
        flexWrap: 'wrap',
        // borderWidth: 10,
    },
    dropdownTitle: {
        fontSize: 15,
        margin: 12,
        color: 'white',
    },
    dropdownText: {
        // flexDirection:'row-reverse' ,
        fontSize: 12,
        margin: 15,
    },
    dropdownImage: {
        width: '15%',
        height: '100%',
        // justifyContent: 'flex-start',
    },
})