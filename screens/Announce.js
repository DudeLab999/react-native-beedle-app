import React, { Component } from 'react';
import {
    View, Text, StyleSheet, ActivityIndicator, ScrollView, Linking
} from "react-native";
import axios from 'axios';

require('../global.js');
const apiUrl = global.API_URL;

export default class Announce extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            promotions: [],
            ...this.props.route.params
        };

    }

    componentDidMount() {
        this.setState({ loading: true })

        if (this.state.lineContact == null || this.state.lineContact == undefined) {
            const config = {
                headers: { Authorization: `Bearer ${this.state.token}` }
            };

            axios.get
                (
                    `${apiUrl}/getLineContact`,
                    config
                )
                .then((response) => {
                    if (response.status == 200) {
                        const result = response.data;

                        let newUrl = (result.lineContact).replace(/g%0A/, '');
                        newUrl = newUrl.replace('\n', '');

                        this.setState({ loading: false, lineContact: newUrl })

                        try {
                            const supported = Linking.canOpenURL(newUrl);

                            if (supported) {
                                Linking.openURL(newUrl);
                            } else {
                                Alert.alert(`Service is not available`);
                            }
                        } catch (err) {
                            console.log(err);
                        }
                    }
                })
                .catch((error) => {
                    console.log(error)
                    this.setState({ loading: false })

                    if (error.response) {
                        if (error.response.status == 401) {
                            Alert.alert(
                                `Session หมดอายุ กรุณาล็อกอินใหม่`,
                                `Session has expired, Please login`,
                            );
                        }
                        this.props.navigation.reset({ index: 0, routes: [{ name: 'Home' }], });
                    }
                });
        } else {
            this.setState({ loading: false })
            try {
                const supported = Linking.canOpenURL(result.lineContact);

                if (supported) {
                    Linking.openURL(result.lineContact);
                } else {
                    Alert.alert(`Service is not available`);
                }
            } catch (err) {
                console.log(err);
            }
        }

        this.props.navigation.goBack();

    }

    render() {
        return (
            <View style={MainStyle.mainContainer}>
                {this.state.loading && <View style={MainStyle.loading}><ActivityIndicator size="large" color="black" /></View>}

                <View style={MainStyle.container}>

                    {/* Promo widget */}
                    <ScrollView style={{ width: '100%' }}>
                        {
                            this.state.promotions != null && this.state.promotions.map((promo, index) => {
                                return (
                                    <View key={index} style={MainStyle.promoWrapper}>
                                        <Text style={MainStyle.promoText} selectable={true}>{promo[0]}</Text>
                                        <Text style={MainStyle.promoActionText} selectable={true}>{promo[1]}</Text>
                                        <Text style={MainStyle.promoPercentText} selectable={true}>{`${promo[2]}%`}</Text>
                                    </View>
                                )
                            })
                        }

                    </ScrollView>

                    {/* End */}


                </View>

            </View >
        )
    }

}

const MainStyle = StyleSheet.create({
    mainContainer: {
        flex: 1,
    },
    loading: {
        position: 'absolute',
        alignSelf: 'center',
        alignItems: 'center',
        zIndex: 100,
        width: '100%',
        height: '100%',
        borderRadius: 10,
        backgroundColor: 'rgba(255,255,255,0.9)',
        justifyContent: 'center',

    },
    loadingText: {
        fontSize: 49
    },
    headerNav: {
        flex: .11,
        width: '100%',
        justifyContent: 'space-between',
        flexDirection: 'row',
        // borderWidth: 1,
        borderColor: 'white',
    },
    profileContainer: {
        marginLeft: 15,
        // borderWidth: 1,
        borderColor: 'white',
        justifyContent: 'center',
    },
    userProfile: {
        width: 35,
        height: 35,
        // borderWidth: 1,
        alignSelf: 'center',
        justifyContent: 'center',
    },
    usernameText: {
        fontSize: 9,
        color: 'white',
        alignSelf: 'center',
        alignContent: 'center',
        justifyContent: 'center',
    },
    mainPageLogoContainer: {
        alignContent: 'center',
        justifyContent: 'center',
        // borderWidth: 1,
        borderColor: 'white',
    },
    mainPageLogo: {
        width: 150,
        alignContent: 'center',
        justifyContent: 'center',
    },
    hamburgerMenu: {
        height: '100%',
        marginRight: '3%',
        alignSelf: 'flex-end',
        alignContent: 'center',
        justifyContent: 'center',
        // borderWidth: 1,
        borderColor: 'white',
    },
    hamburgerIcon: {
        width: 50,
        alignSelf: 'center',
        alignContent: 'center',
        justifyContent: 'center',
    },
    container: {
        flex: .9,
        marginTop: 45,
        alignItems: 'center',
        // borderWidth: 1,
        borderColor: 'green',
    },
    footerBackground: {
        position: 'absolute',
        bottom: 0,
        borderColor: '#426177',
        borderWidth: 1,
        width: '100%',
        height: 65,
        zIndex: -10,
    },
    footerMainButtonImage: {
        width: '100%',
        alignSelf: "center",
        justifyContent: 'center',
        height: '100%'
    },
    footerButtonImage: {
        width: 30,
        alignSelf: "center",
        justifyContent: 'center',
        // borderWidth: 1,
        height: 30
    },
    footer: {
        flex: .2,
        flexDirection: 'row',
        // flexWrap: 'wrap',
        justifyContent: 'space-between',
        alignContent: 'center',
        // borderWidth: 2,
        borderColor: 'white',
    },
    footerButton: {
        height: 70,
        width: '18%',
        borderRadius: 15,
        flexDirection: 'column',
        alignSelf: 'flex-end',
        alignContent: 'center',
        justifyContent: 'center',
        // borderWidth: 1,
        borderColor: 'white',
    },
    footerIcon: {
        fontSize: 10,
        color: 'white',
        justifyContent: 'center',
        alignSelf: 'center',
    },
    footerMain: {
        height: 90,
        width: '22%',
        borderRadius: 360,
        marginTop: 10,
        alignContent: 'center',
        justifyContent: 'center',
        borderColor: 'white',
        zIndex: 1,
    },
    promoWrapper: {
        flex: .1,
        width: '100%',
        marginTop: '5%',
        borderWidth: 1,
        borderColor: '#ff943e',
        alignSelf: 'center',
        justifyContent: 'center',
    },
    promoText: {
        fontSize: 29,
        fontWeight: '700',
        color: '#0fc2ff',
        textAlign: 'center',
        fontFamily: 'sukhumvitset-text-webfont',
        alignSelf: 'center',
        alignContent: 'center',
        justifyContent: 'center',
    },
    promoActionText: {
        fontSize: 24,
        fontWeight: '600',
        color: 'white',
        textAlign: 'center',
        fontFamily: 'sukhumvitset-bold-webfont',
        alignSelf: 'center',
        alignContent: 'center',
        justifyContent: 'center',
    },
    promoPercentText: {
        fontSize: 64,
        fontWeight: '600',
        color: '#0fc2ff',
        textAlign: 'center',
        fontFamily: 'sukhumvitset-text-webfont',
        alignSelf: 'center',
        alignContent: 'center',
        justifyContent: 'center',
    },

});


