import { View, Text, StyleSheet, Linking } from "react-native"
import Login from '../components/Login';
import { LinearGradient } from 'expo-linear-gradient';
import { TouchableOpacity } from 'react-native-gesture-handler'

require('../global.js');

let lineUrl = global.lineUrl;

const apiUrl = global.API_URL;


const openChat = () => {
    try {
        const supported = Linking.canOpenURL(lineUrl);

        if (supported) {
            Linking.openURL(lineUrl);
        } else {
            Alert.alert(`Service is not available`, lineUrl);
        }
    } catch (err) {
        console.log(err);
    }
}



const Home = ({ route, navigation }) => {
    return (

        <View style={registerStyle.container}>
            {/* <ImageSlide /> */}
            <Login style={registerStyle.loginContainer} liveChat={() => openChat()} LoginSuccess={(props) => navigation.navigate('Main', props)} Register={() => navigation.navigate('PhoneRegister')} />

            <LinearGradient style={registerStyle.liveChatButtonContainer} colors={['#0078FF', '#00C6FF', '#00C6FF']}>
                {/* <View style={registerStyle.sp}> */}
                <TouchableOpacity
                    onPress={() => openChat()}
                    // onPress={() => navigation.navigate('Chat')}
                    style={registerStyle.liveChatButton}>
                    <Text style={registerStyle.liveChatText}>Live Chat!</Text>
                </TouchableOpacity>
                {/* </View> */}
            </LinearGradient>
        </View>
    )
}


export default Home;

const registerStyle = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: '10%',
        justifyContent: 'center',
        // borderWidth:1,
        // borderColor: 'blue'
    },
    buttonRegister: {
        marginBottom: 90,
        height: 70,
        textAlign: 'center',
        fontFamily: 'sukhumvitset-thin-webfont',
        marginHorizontal: 20,
        justifyContent: 'center',
        zIndex: 1,
    },
    liveChatButtonContainer: {
        // marginTop:'-5%',
        marginBottom: '20%',
        alignSelf: 'flex-end',
        alignContent: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        borderColor: 'white',
        borderRadius: 40,
        width: 80,
        marginRight: 20,
        height: 80,
        zIndex: 999,
    },
    liveChatButton: {
        width: 80,
        height: 80,
        justifyContent: 'center',
        alignItems: 'center',
    },
    liveChatText: {
        fontSize: 15,
        color: 'white',
        textAlign: 'center',
        fontFamily: 'sukhumvitset-thin-webfont',
    },
    textButton: {
        fontSize: 15,
        color: 'white',
        textAlign: 'center',
        fontFamily: 'sukhumvitset-thin-webfont',
    },
    inputContainer: {
        flex: 3,
        marginTop: -80,
        zIndex: 1,
    },
    linearGradient: {
        flex: 1,
        justifyContent: 'center',
        alignContent: 'center',
        marginBottom: 10,
    },
    linearButtonContainer: {
        alignSelf: 'center',
        justifyContent: 'center',
        alignContent: 'center',
        width: '100%',
        height: 50
    }
})