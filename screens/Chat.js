import React, { Component } from 'react';
import { GiftedChat } from 'react-native-gifted-chat';
import { View, Text, StyleSheet, TouchableOpacity } from "react-native"

const styleChat = StyleSheet.create({
    chatPage: {
        flex: 1,
        borderWidth: 1,
        backgroundColor: 'transparent',
        bottom: 0,
    },
    chatTopSpace: {
        height: 200,
    },
    chatContainer: {
        height: 40,
        borderRadius: 0,
        backgroundColor: 'white',
        borderColor: 'black',
        borderWidth: 0.2,
        flexDirection: 'row',
    },
    backButtonContainer: {
        margin: 10,
        width: 80,
        justifyContent: 'center',
        alignSelf: 'center',
    },
    backButton: {
        borderWidth: 1,
    },
    backButtonText: {
        color: '#0084ff',
    },
    chatTitle: {
        alignSelf: 'center',
    }
})

let id = 0

let messagesOutside = []

export default class Chat extends Component {
    constructor(props) {
        super(props)

        this.state = {
            navigation: this.props.navigation,
            messages: [
                {
                    _id: 1,
                    text: "Hi, It's nice cup 😊",
                    createdAt: new Date(),
                    user: {
                        _id: 2,
                        name: 'Nice cup',
                        avatar: 'https://placeimg.com/140/140/any',
                    },
                },
            ],
        }
    }

    showHide = () => {
        this.state.navigation.goBack()
    }

    getPreviousMessges = () => {
        this.setState(prevState => (
            {
                messages: prevState.messages
            }
        ))
    }

    componentDidMount() {
        if (id != 0) {
            this.setState({
                messages: messagesOutside
            })
        } else {
            this.setState({
                messages: this.state.messages
            })
        }
    }

    onSend = (messages = []) => {
        this.setState(previousState => ({
            messages: GiftedChat.append(previousState.messages, messages),
        }))
    }

    componentWillUnmount() {
        messagesOutside = this.state.messages
        id = 1
    }

    render() {
        return (

            <View style={styleChat.chatPage}>
                <View style={styleChat.chatTopSpace}></View>

                <View style={styleChat.chatContainer}>

                    <TouchableOpacity style={styleChat.backButtonContainer} onPress={() => this.showHide()}>
                        <Text style={styleChat.backButtonText}>Back</Text>
                    </TouchableOpacity>

                    <Text style={styleChat.chatTitle}>Super Cup Live Chat</Text>

                </View>

                <GiftedChat
                    messagesContainerStyle={{ backgroundColor: 'white' }}
                    messages={this.state.messages}
                    onSend={messages => this.onSend(messages)}
                    user={{
                        _id: 1,
                    }}
                    // loadEarlier={true}
                    onChangeText={(messages) => this.setState({ messages })}
                    value={this.state.messages}

                />
            </View>
        )
    }
}

