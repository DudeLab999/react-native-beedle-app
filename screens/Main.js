import React, { Component } from 'react';
import {
    View, Text, StyleSheet, Linking, ActivityIndicator,
    Image, TouchableOpacity, Modal, TouchableWithoutFeedback, Alert,
} from "react-native";

import { LinearGradient } from 'expo-linear-gradient';
import Profile from '../components/Profile';
import PlayAccess from '../components/PlayAccess';
import Landing from '../components/Landing';

require('../global.js');
import axios from 'axios';

const DEPOSIT = 0;
const WITHDRAW = 1;

const apiUrl = global.API_URL;

class Main extends Component {

    constructor(props) {
        super(props)

        const auth = this.props.route.params;

        this.state = {
            debug: auth.debug,
            token: auth.access_token,
            refreshToken: null,
            expireIn: auth.expires_in,
            loading: false,

            firstname: null,
            lastname: null,
            nickname: null,
            username: null,
            surname: null,
            phone: null,
            userBankLogo: require('../assets/img/bank_icons/kbank.png'),
            userBankCode: null,
            userBankName: null,
            userBankNo: null,
            cupWalletLogo: require('../assets/img/bank_icons/kbank.png'),
            cupWalletName: null,
            cupWalletNo: null,
            cupWalletOwner: null,
            balance: 0,

            // Pages
            mainPage: true,
            transactionPage: false,
            linkRubSub: false,
            modalVisible: false,
            profile: false,
            playAccess: false,

            // Game
            gameType: null,
        }

    }

    componentDidMount() {
        console.log("log", this.state.debug)
        if (this.state.debug) {
            this.setState(({
                debugMode: true,
                lineContact: '',
                firstname: "John",
                lastname: "Doh",
                nickname: "John",
                username: "Tester Free Package",
                surname: `${"John"} ${"Doh"}`,
                phone: "+xx-xx-x71x-xxx",

                bankName: "scb",
                userBankName: "John Doh",
                userBankNo: "0-xxxx-xx-x-xx",
                bankNo: "0-xxxx-xx-x-xx",

                cupWalletCode: "scb",
                cupWalletName: "Master Payment",
                cupWalletNo: "0-xxxx-xx-x-14",
                cupWalletOwner: "Beedle Payment",

                commission: 0,
                balance: 0,
            }))
        }
        else {
            const config = {
                headers: { Authorization: `Bearer ${this.state.token}` }
            };

            axios.post
                (
                    `${apiUrl}/me`,
                    {},
                    config
                )
                .then((response) => {
                    if (response.status == 200) {
                        const result = response.data;

                        this.setState(
                            (
                                {
                                    lineContact: result.data.lineContact,
                                    firstname: result.data.firstname,
                                    lastname: result.data.lastname,
                                    nickname: result.data.nickname,
                                    username: result.data.username,
                                    surname: `${result.data.firstname} ${result.data.lastname}`,
                                    phone: result.data.phone_number,

                                    userBankCode: result.userBank.userBankCode,
                                    userBankName: result.userBank.userBankName,
                                    userBankNo: result.userBank.bankNo,

                                    cupWalletCode: result.userBank.cupWalletCode,
                                    cupWalletName: result.userBank.cupWalletName,
                                    cupWalletNo: result.userBank.cupWalletNo,
                                    cupWalletOwner: result.userBank.cupWalletOwner,

                                    commission: result.data.commission,
                                    balance: result.availableCredit,
                                }
                            )
                        );
                    }
                })
                .catch((error) => {
                    if (error.response) {
                        if (error.response.status == 401) {
                            Alert.alert(
                                `กรุณาล็อกอินใหม่`,
                                `Please login`,
                            );
                        }
                        this.props.navigation.reset({ index: 0, routes: [{ name: 'Home' }], });
                    }
                });
        }
    }

    setModalVisible = (status) => {
        this.setState(
            (
                {
                    modalVisible: status
                }
            )
        );
    }

    showTransactionPage = (status, flag, from) => {
        if (flag === undefined || flag === null) {
            flag = 0;
        }
        this.setState(
            (
                {
                    mainPage: true,
                    modalVisible: false,
                    transactionPageActive: flag,
                    linkRubSub: false,
                    playAccess: false,
                }
            )
        );
        this.props.navigation.navigate("Transaction", { ...this.state, transactionPageActive: flag });
    }

    showMainPage = (status) => {
        this.setState(
            (
                {
                    mainPage: status,
                    transactionPage: false,
                    modalVisible: false,
                    linkRubSub: false,
                    profile: false,
                    playAccess: false,
                }
            )
        );
    }

    showProfile = () => {
        this.setState(
            (
                {
                    profile: true,
                    linkRubSub: false,
                    mainPage: false,
                    transactionPage: false,
                    modalVisible: false,
                    playAccess: false,
                }
            )
        );
    }

    refreshBalance = () => {
        const config = {
            headers: { Authorization: `Bearer ${this.state.token}` }
        };

        axios.post
            (
                `${apiUrl}/getCredit`,
                {
                    "username": this.state.username
                },
                config
            )
            .then((response) => {
                if (response.status == 200) {
                    console.log(response.data);
                    console.log(response.data.credit);
                    this.setState(
                        (
                            {
                                balance: response.data.credit,
                            }
                        )
                    );
                }
            })
            .catch((error) => {
                if (error.response) {
                    if (error.response.status == 401) {
                        Alert.alert(
                            `กรุณาล็อกอินใหม่`,
                            `Please login`,
                        );
                    }
                    this.props.navigation.reset({ index: 0, routes: [{ name: 'Home' }], });
                }
            });

    }

    setDefaultState = () => {
        this.setState(
            (
                {
                    token: null,
                    refreshToken: null,
                    linkRubSub: false,
                    transactionPage: false,
                    mainPage: false,
                    transactionPage: true,
                    modalVisible: false,
                    profile: false,
                    playAccess: false,
                }
            )
        );
    }

    showGameList = (game) => {
        this.setState(
            (
                {
                    modalVisible: false,
                }
            )
        );

        if (game == "sport") {
            game = "obet";
            this.play(game);
        } else if (game == "greenTea") {
            game = "amblotto";
            this.play(game);
        } else {
            this.props.navigation.navigate("PlayAccess", {
                ...this.state,
                gameType: game
            });
        }

    }

    logOut = () => {
        this.props.navigation.reset({ index: 0, routes: [{ name: 'Home' }], });
    }

    play = async (game) => {
        this.setState(
            (
                {
                    loading: true
                }
            )
        );

        const config = {
            headers: { Authorization: `Bearer ${this.state.token}` }
        };

        const payload = {
            "game": game
        }

        if (game == "amblotto") {
            payload["amblotto"] = true;
        } else {
            payload["chaiyo"] = true;
        }

        axios.post
            (
                `${apiUrl}/play`,
                payload,
                config
            )
            .then((response) => {
                this.setState(
                    (
                        {
                            loading: false
                        }
                    )
                );
                if (response.status == 200) {

                    const url = response.data.url;
                    const supported = Linking.canOpenURL(url);

                    if (supported) {
                        Linking.openURL(url);
                    } else {
                        Alert.alert(`ไม่สามารถเปิด link ได้ : ${url}`);
                    }
                }
            })
            .catch((error) => {
                console.log(error);
                this.setState(
                    (
                        {
                            loading: false
                        }
                    )
                );
                if (error.response) {
                    if (error.response.status == 401) {
                        Alert.alert(
                            `กรุณาล็อกอินใหม่`,
                            `Please login`,
                        );
                        this.props.navigation.reset({ index: 0, routes: [{ name: 'Home' }], });
                    }
                    else if (error.response.status == 500) {
                        Alert.alert(
                            `Attention`,
                            `Oops! Try again next time`,
                        );
                    }

                }
            });
    }

    changePassword = () => {
        this.props.navigation.navigate("ChangePassword", {
            ...this.state
        });
    }

    componentWillUnmount() {
        const config = {
            headers: { Authorization: `Bearer ${this.state.token}` }
        };
        axios.post
            (
                `${apiUrl}/logout`,
                {},
                config
            )
            .then((response) => {
            })
            .catch((error) => {
            });
    }

    render() {
        let returnView = {};

        if (this.state.debugMode) {
            return <Landing initial={this.state} {...this.props} logout={() => this.logOut()} />
        }

        const profileWidgets = <Profile
            changePassword={() => this.changePassword()}
            logout={() => this.logOut()}
            firstname={this.state.firstname}
            lastname={this.state.lastname}
            nickname={this.state.nickname}
            username={this.state.username}
            phone={this.state.phone}
            bankNo={this.state.userBankNo}
            bankName={this.state.userBankName}
            bankCode={this.state.userBankCode}
            {...this.props}
        />

        //showGameList
        const playAccessWidget = <PlayAccess
            gameType={this.state.gameType}
            play={() => this.play}
            {...this.props}
        />


        const mainWidgets = (
            <>
                {/* MAIN WIDGETS */}

                {/* score Background */}
                <View style={MainStyle.scoreBackgroundContainer}>
                    <View style={MainStyle.scoreBackgroundWrapper}>
                        <Image
                            resizeMode='contain'
                            source={require("../assets/img/login_bg.png")}
                            style={MainStyle.scoreBackground}
                        />
                        <Text style={MainStyle.creditTitle}>ยอดเครดิต</Text>
                        <Text style={MainStyle.creditText}>{this.state.balance}</Text>

                        <TouchableOpacity style={MainStyle.refreshIconButton}
                            onPress={() => this.refreshBalance()}
                        >
                            <Image
                                resizeMode='contain'
                                source={require("../assets/img/refresh_icon.png")}
                                style={MainStyle.refreshIcon}
                            />
                        </TouchableOpacity>

                    </View>

                </View>

                <View style={MainStyle.menuContainer}>
                    <View style={MainStyle.topMenu}>

                        <LinearGradient
                            colors={['#223041', '#212529', '#426177']}
                            start={[1, 0]}
                            end={[1, 1]}
                            style={MainStyle.topMenuButton}
                        >
                            <TouchableOpacity
                            >
                                <Text style={MainStyle.topMenuButtonText}>Cup draw</Text>
                            </TouchableOpacity>
                        </LinearGradient>

                        <LinearGradient
                            colors={['#223041', '#212529', '#426177']}
                            start={[1, 0]}
                            end={[1, 1]}
                            style={MainStyle.topMenuButton}
                        >
                            <TouchableOpacity
                            >
                                <Text style={MainStyle.topMenuButtonText}>Remove cup</Text>
                            </TouchableOpacity>
                        </LinearGradient>

                        <LinearGradient
                            colors={['#223041', '#212529', '#426177']}
                            start={[1, 0]}
                            end={[1, 1]}
                            style={MainStyle.topMenuButtonLong}
                        >
                            <TouchableOpacity
                            >
                                <Text style={MainStyle.topMenuButtonText}>Rewards meta</Text>
                            </TouchableOpacity>
                        </LinearGradient>

                    </View>

                    <View style={MainStyle.gameMenu}>

                        <TouchableOpacity
                            style={MainStyle.gameButtonWrapper}
                            onPress={() => this.showGameList("cuddle")}
                        >
                            <Image
                                resizeMode='contain'
                                source={require("../assets/img/cappu.png")}
                                style={MainStyle.gameButtonIcon}
                            />
                            <Text style={MainStyle.gameButtonText}>Cuddle</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={MainStyle.gameButtonWrapper}
                            onPress={() => this.showGameList("cuddle")}
                        >
                            <Image
                                resizeMode='contain'
                                source={require("../assets/img/cuddle.png")}
                                style={MainStyle.gameButtonIcon}
                            />
                            <Text style={MainStyle.gameButtonText}>Cappu</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={MainStyle.gameButtonWrapper}
                            onPress={() => this.showGameList("chooseCup")}
                        >
                            <Image
                                resizeMode='contain'
                                source={require("../assets/img/cuphidden.png")}
                                style={MainStyle.gameButtonIcon}
                            />
                            <Text style={MainStyle.gameButtonText}>Choose Cup</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={MainStyle.gameButtonWrapper}
                            onPress={() => this.showGameList("cappu")}
                        >
                            <Image
                                resizeMode='contain'
                                source={require("../assets/img/cappu.png")}
                                style={MainStyle.gameButtonIcon}
                            />
                            <Text style={MainStyle.gameButtonText}>Cappu</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={MainStyle.gameButtonWrapper}
                            onPress={() => this.showGameList("greenTea")}
                        >
                            <Image
                                resizeMode='contain'
                                source={require("../assets/img/green-tea.png")}
                                style={MainStyle.gameButtonIcon}
                            />
                            <Text style={MainStyle.gameButtonText}>Green Tea</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={MainStyle.gameButtonWrapper}
                            onPress={() => this.showProfile()}
                        >
                            <Image
                                resizeMode='contain'
                                source={require("../assets/img/account_bt.png")}
                                style={MainStyle.gameButtonIcon}
                            />
                            <Text style={MainStyle.gameButtonText}>บัญชีผู้ใช้</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={MainStyle.gameButtonWrapper}
                            onPress={() => this.changePassword()}
                        >
                            <Image
                                resizeMode='contain'
                                source={require("../assets/img/change_password_bt.png")}
                                style={MainStyle.gameButtonIcon}
                            />
                            <Text style={MainStyle.gameButtonText}>Change Password</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={MainStyle.gameButtonWrapper}
                            onPress={() => this.logOut()}
                        >
                            <Image
                                resizeMode='contain'
                                source={require("../assets/img/logout_bt.png")}
                                style={MainStyle.gameButtonIcon}
                            />
                            <Text style={MainStyle.gameButtonText}>Logout</Text>
                        </TouchableOpacity>


                    </View>
                </View>
            </>
        );

        returnView = (
            <View style={MainStyle.mainContainer}>
                {this.state.loading && <View style={MainStyle.loading}><ActivityIndicator size="large" color="black" /></View>}
                <View style={MainStyle.container}>
                    <View style={MainStyle.headerNav}>
                        <View style={MainStyle.profileContainer}>
                            <Image
                                resizeMode='contain'
                                source={require("../assets/img/account_bt.png")}
                                style={MainStyle.userProfile}
                            />
                            <Text style={MainStyle.usernameText}>บัญชีผู้ใช้ใหม่</Text>
                            <Text style={MainStyle.usernameText}>{this.state.username}</Text>
                        </View>

                        <TouchableOpacity style={MainStyle.mainPageLogoContainer}
                            onPress={() => this.showMainPage(true)}
                        >
                            <Image
                                resizeMode='contain'
                                source={global.LOGO}
                                style={MainStyle.mainPageLogo}
                            />
                        </TouchableOpacity>

                        <TouchableOpacity style={MainStyle.hamburgerMenu}
                            onPress={() => this.setModalVisible(true)}
                        >
                            <Image
                                resizeMode='contain'
                                source={require("../assets/img/right_top_menu.png")}
                                style={MainStyle.hamburgerIcon}
                            />

                        </TouchableOpacity>

                    </View>

                    {/* MODAL */}
                    <Modal
                        animationType="fade"
                        transparent={true}
                        visible={this.state.modalVisible}
                        onRequestClose={() => { this.setModalVisible(false) }}
                    >
                        <TouchableOpacity
                            style={modalStyle.modalContainer}
                            activeOpacity={1}
                            onPressOut={() => { this.setModalVisible(false) }}
                        >

                            <TouchableWithoutFeedback>
                                <View style={modalStyle.modalContainerMain}>
                                    {/* Small icon */}
                                    <View style={modalStyle.smallButtonWrapper}>
                                        <TouchableOpacity style={modalStyle.modalCardContainerSmall}
                                            onPress={() => this.showGameList("cuddle")}
                                        >
                                            <Image
                                                resizeMode='contain'
                                                source={require("../assets/img/cuddle.png")}
                                                style={modalStyle.modalCardIcon}
                                            />
                                            <Text style={modalStyle.modalCardText}>Cuddle</Text>
                                        </TouchableOpacity>

                                        <TouchableOpacity style={modalStyle.modalCardContainerSmall}
                                            onPress={() => this.showGameList("cuddle")}
                                        >
                                            <Image
                                                resizeMode='contain'
                                                source={require("../assets/img/cuddle.png")}
                                                style={modalStyle.modalCardIcon}
                                            />
                                            <Text style={modalStyle.modalCardText}>Cappu</Text>
                                        </TouchableOpacity>

                                        <TouchableOpacity style={modalStyle.modalCardContainerSmall}
                                            onPress={() => this.showGameList("cappu")}
                                        >
                                            <Image
                                                resizeMode='contain'
                                                source={require("../assets/img/cappu.png")}
                                                style={modalStyle.modalCardIcon}
                                            />
                                            <Text style={modalStyle.modalCardText}>Cappuchino</Text>
                                        </TouchableOpacity>

                                        <TouchableOpacity style={modalStyle.modalCardContainerSmall}
                                            onPress={() => this.showGameList("greenTea")}
                                        >
                                            <Image
                                                resizeMode='contain'
                                                source={require("../assets/img/green-tea.png")}
                                                style={modalStyle.modalCardIcon}
                                            />
                                            <Text style={modalStyle.modalCardText}>Green Tea</Text>
                                        </TouchableOpacity>

                                        <TouchableOpacity style={modalStyle.modalCardContainerSmall}
                                            onPress={() => this.showGameList("chooseCup")}
                                        >
                                            <Image
                                                resizeMode='contain'
                                                source={require("../assets/img/cuphidden.png")}
                                                style={modalStyle.modalCardIcon}
                                            />
                                            <Text style={modalStyle.modalCardText}>Choose Cup</Text>
                                        </TouchableOpacity>
                                    </View>


                                </View>
                            </TouchableWithoutFeedback>

                        </TouchableOpacity>
                    </Modal>
                    {/* END MODAL */}

                    {/* Main Widgets */}
                    {this.state.mainPage && mainWidgets}
                    {/* END Main Widgets */}

                    {/* Profile Widget */}
                    {this.state.profile && profileWidgets}
                    {/* End Profile Widget */}

                </View>




                {/* Footer */}
                <View style={MainStyle.footer}>
                    <TouchableOpacity style={MainStyle.footerButton}
                        onPress={() => this.showMainPage(true)}
                    >
                        <Image
                            resizeMode='contain'
                            source={require("../assets/img/home_bottom_icon.png")}
                            style={MainStyle.footerButtonImage}
                        />
                        <Text style={MainStyle.footerIcon}>Home</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate("Promotion", { ...this.state })}
                        style={MainStyle.footerButton}>
                        <Image
                            resizeMode='contain'
                            source={require("../assets/img/promotion_bottom_icon.png")}
                            style={MainStyle.footerButtonImage}
                        />
                        <Text style={MainStyle.footerIcon}>Promotion</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={MainStyle.footerMain}
                        onPress={() => this.showMainPage(true)}
                    >
                        <Image
                            resizeMode='contain'
                            source={require("../assets/img/cup_bt.png")}
                            style={MainStyle.footerMainButtonImage}
                        />
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => this.props.navigation.navigate("Announce", { ...this.state })}
                        style={MainStyle.footerButton}>
                        <Image
                            resizeMode='contain'
                            source={require("../assets/img/announce_bottom_bt.png")}
                            style={MainStyle.footerButtonImage}
                        />
                        <Text style={MainStyle.footerIcon}>Announcement</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={MainStyle.footerButton}
                        onPress={() => this.props.navigation.navigate("Announce", { ...this.state })}
                    // onPress={() => this.props.navigation.navigate('Chat')}
                    >
                        <Image
                            resizeMode='contain'
                            source={require("../assets/img/contact_bottom_bt.png")}
                            style={MainStyle.footerButtonImage}
                        />
                        <Text style={MainStyle.footerIcon}>Contact</Text>
                    </TouchableOpacity>
                </View>


                <LinearGradient style={MainStyle.footerBackground}
                    colors={['#223041', '#212529', '#426177']}
                    start={[1, 0]}
                    end={[1, 1]}
                >
                </LinearGradient>

            </View>
        )

        return returnView;

    }

}

export default Main;

const modalStyle = StyleSheet.create({
    modalContainer: {
        // borderWidth: 2,
        // backgroundColor: 'white',
        height: '100%',
    },
    modalContainerMain: {
        marginTop: '20%',
        width: '100%',
        height: 300,
        flexWrap: 'wrap',
        flexDirection: 'row',
        alignSelf: 'center',
        alignContent: 'center',
        justifyContent: 'space-evenly',
        borderWidth: 1,
        borderColor: '#426177',
        backgroundColor: '#223041',
    },
    modalCardContainer: {
        width: 100,
        height: 100,
        // alignSelf: 'center',
        justifyContent: 'center',
        alignContent: 'center',
        borderWidth: 1,
        borderColor: '#426177',
        borderRadius: 5,
    },
    modalCardContainerSmall: {
        width: 60,
        height: 90,
        margin: 5,
        // alignSelf: 'center',
        justifyContent: 'center',
        alignContent: 'center',
        borderWidth: 1,
        borderColor: '#426177',
        borderRadius: 5,
    },
    modalCardIcon: {
        width: 40,
        height: 40,
        alignSelf: 'center',
        alignContent: 'center',
        justifyContent: 'center',
    },
    modalCardText: {
        fontFamily: 'sukhumvitset-text-webfont',
        textAlign: 'center',
        marginTop: 10,
        color: 'white',
        alignSelf: 'center',
        alignContent: 'center',
        justifyContent: 'center',
    },
    smallButtonWrapper: {
        // borderWidth: 1,
        marginTop: 5,
        width: '100%',
        // flexWrap: 'wrap',
        flexDirection: 'row',
        // alignSelf: 'center',
        // alignContent: 'center',
        justifyContent: 'space-evenly',
    },
});

const MainStyle = StyleSheet.create({
    loading: {
        position: 'absolute',
        alignSelf: 'center',
        // bottom: '50%',
        alignItems: 'center',
        zIndex: 100,
        width: '100%',
        height: '100%',
        // borderWidth: 1,
        borderRadius: 10,
        backgroundColor: 'rgba(255,255,255,0.9)',
        justifyContent: 'center',

    },
    loadingText: {
        fontSize: 49
    },
    mainContainer: {
        flex: 1,
        // borderWidth: 1,
        // borderColor: 'white',
    },
    headerNav: {
        flex: .11,
        width: '100%',
        justifyContent: 'space-between',
        flexDirection: 'row',
        // borderWidth: 1,
        borderColor: 'white',
    },
    profileContainer: {
        marginLeft: 15,
        // borderWidth: 1,
        borderColor: 'white',
        justifyContent: 'center',
    },
    userProfile: {
        width: 35,
        height: 35,
        // borderWidth: 1,
        alignSelf: 'center',
        justifyContent: 'center',
    },
    usernameText: {
        fontFamily: 'sukhumvitset-text-webfont',
        textAlign: 'center',
        fontSize: 9,
        color: 'white',
        alignSelf: 'center',
        alignContent: 'center',
        justifyContent: 'center',
    },
    mainPageLogoContainer: {
        alignContent: 'center',
        justifyContent: 'center',
        // borderWidth: 1,
        borderColor: 'white',
    },
    mainPageLogo: {
        width: 150,
        alignContent: 'center',
        justifyContent: 'center',
    },
    hamburgerMenu: {
        height: '100%',
        marginRight: '3%',
        alignSelf: 'flex-end',
        alignContent: 'center',
        justifyContent: 'center',
        // borderWidth: 1,
        borderColor: 'white',
    },
    hamburgerIcon: {
        width: 50,
        alignSelf: 'center',
        alignContent: 'center',
        justifyContent: 'center',
    },
    container: {
        flex: .9,
        marginTop: 45,
        alignItems: 'center',
        // borderWidth: 1,
        borderColor: 'green',
    },
    footerBackground: {
        position: 'absolute',
        bottom: 0,
        borderColor: '#426177',
        borderWidth: 1,
        width: '100%',
        height: 65,
        zIndex: -10,
    },
    footerMainButtonImage: {
        width: '100%',
        alignSelf: "center",
        justifyContent: 'center',
        height: '100%'
    },
    footerButtonImage: {
        width: 30,
        alignSelf: "center",
        justifyContent: 'center',
        // borderWidth: 1,
        height: 30
    },
    footer: {
        flex: .15,
        flexDirection: 'row',
        // flexWrap: 'wrap',
        justifyContent: 'space-between',
        alignContent: 'center',
        // borderWidth: 2,
        borderColor: 'white',
    },
    footerButton: {
        height: 70,
        width: '18%',
        borderRadius: 15,
        flexDirection: 'column',
        alignSelf: 'flex-end',
        alignContent: 'center',
        justifyContent: 'center',
        // borderWidth: 1,
        borderColor: 'white',
    },
    footerIcon: {
        fontSize: 10,
        color: 'white',
        justifyContent: 'center',
        alignSelf: 'center',
    },
    footerMain: {
        height: 90,
        width: '22%',
        borderRadius: 360,
        marginTop: 10,
        alignContent: 'center',
        justifyContent: 'center',
        borderColor: 'white',
        zIndex: 1,
    },
    title: {
        margin: 12,
        alignSelf: 'flex-start',
        fontSize: 15,
        color: 'white',

    },
    scoreBackgroundContainer: {
        flex: 0.5,
        width: '100%',
        justifyContent: 'center',
        // alignContent: 'center',
        // borderWidth: 1,
    },
    scoreBackgroundWrapper: {
        height: '100%',
        width: '100%',
        // borderWidth: 1,
        borderColor: 'white',
    },
    scoreBackground: {
        marginLeft: '10%',
        height: '90%',
        width: '90%',
        alignSelf: 'center',
        // borderWidth: 1,
        borderColor: 'blue',
    },
    refreshIconButton: {
        position: 'absolute',
        top: '60%',
        left: '65%',
        width: '100%'
    },
    refreshIcon: {
        width: '15%'
    },
    creditTitle: {
        fontFamily: 'sukhumvitset-bold-webfont',
        position: 'absolute',
        color: 'white',
        alignSelf: 'center',
        top: '30%',
        fontSize: 29,
        fontWeight: '600',
    },
    creditText: {
        fontFamily: 'sukhumvitset-thin-webfont',
        position: 'absolute',
        color: 'white',
        alignSelf: 'center',
        top: '40%',
        fontSize: 59,
        fontWeight: '500',
    },

    // Main Menu
    menuContainer: {
        flex: 0.5,
        width: '100%',
        // borderWidth: 1,
        borderColor: 'blue',
    },
    topMenu: {
        flex: 0.4,
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        // borderWidth: 1
    },
    gameMenu: {
        flex: 0.6,
        flexDirection: 'row',
        flexWrap: 'wrap',
        // borderWidth: 1
    },
    topMenuButton: {
        width: '45%',
        height: 40,
        margin: 5,
        borderRadius: 5,
        borderColor: '#426177',
        alignContent: 'center',
        justifyContent: 'center',
        borderWidth: 1,
    },
    topMenuButtonText: {
        color: 'white',
        fontSize: 19,
        fontFamily: 'sukhumvitset-text-webfont',
        textAlign: 'center',
        alignSelf: 'center',
        alignContent: 'center',
        justifyContent: 'center',
    },
    topMenuButtonLong: {
        width: '90%',
        height: 40,
        borderRadius: 5,
        borderColor: '#d6c981',
        alignSelf: 'center',
        alignContent: 'center',
        justifyContent: 'center',
        borderWidth: 1,
    },
    gameButtonWrapper: {
        width: '25%',
        height: '40%',
        justifyContent: 'center',
        alignContent: 'center',
        // borderWidth: 3,
    },
    gameButtonIcon: {
        width: '100%',
        height: '70%',
        alignSelf: 'center',
        alignContent: 'center',
        justifyContent: 'center',
    },
    gameButtonText: {
        fontSize: 11,
        fontFamily: 'sukhumvitset-text-webfont',
        textAlign: 'center',
        marginTop: 5,
        color: 'white',
        alignSelf: 'center',
        alignContent: 'center',
        justifyContent: 'center',
    }

})