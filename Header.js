import React, { Component } from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import Swiper from 'react-native-swiper'
require('./global.js');

const styles = StyleSheet.create({
  headerContainer: {
    marginTop: 100,
    flex: 1,
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: "center",
    alignSelf: "center",
    // borderWidth: 2,
    // justifyContent: 'space-between'
  },
  headerLogo: {
    width: 250,
    alignSelf: "center",
  },
  h1: {

  },
  h2: {

  },
  baseText: {
    fontWeight: 'bold'
  },
  innerText: {
    color: 'white'
  },
  textCenter: {
    color: 'white',
    alignContent: 'center',
    alignItems: 'center'
  },
  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold'
  },

  headerRegister: {
    marginTop: 120,
    marginRight: 'auto',
    flex: .1,
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: "center",
    alignSelf: "center",
    // borderWidth:2,
  },
  headerLogoRegister: {
    width: 220,
    alignSelf: "center",
  },
});

export default class Header extends Component {
  constructor(props) {
    super(props);

    this.state = {
      logoLocation: global.LOGO
    }

  }

  render() {
    return (

      <View style={this.props.Register ? styles.headerRegister : styles.headerContainer}>
        <Image
          resizeMode="contain"
          source={this.state.logoLocation}
          style={this.props.Register ? styles.headerLogoRegister : styles.headerLogo}
        />
      </View>
    )
  }
}

