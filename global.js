// App name
global.APP_NAME = "Beedle";
global.APP_URL = "";

global.LOGO = require('./assets/img/cuddle.png');

// apiUrl
global.API_URL = "https://beedle.metacity/api/v2";

// Banks
global.banks = {
    "scb": 'ธนาคารไทยพาณิชย์',
    "kbank": 'ธนาคารกสิกรไทย',
    "ktb": 'ธนาคารกรุงไทย',
    "bbl": 'ธนาคารกรุงเทพ',
    "bay": 'ธนาคารกรุงศรี',
    "tbank": 'ธนาคารธนชาต',
    "tmb": 'ธนาคารทหารไทย',
    "gsb": 'ธนาคารออมสิน',
    "bag": 'ธนาคารเพื่อการเกษตรและสหกรณ์การเกษตร'
}

// Line url
global.lineUrl = "";

global.registerToken = "";