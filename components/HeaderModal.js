import React, { Component } from 'react';
import {
    View, Text, StyleSheet,
    Image, TouchableOpacity, Modal, TouchableWithoutFeedback
} from "react-native";

const DEPOSIT = 0;
const WITHDRAW = 1;
const HISTORY = 2;
const BANK_TRANSFER = 0;
const QR_PAYMENT = 1;

export default class HeaderModal extends Component {
    constructor(props) {
        super(props);

        this.state = {
            modalVisible: true
        }
    }

    setModalVisible = (status) => {
        this.setState(
            (
                {
                    modalVisible: status
                }
            )
        );
    }

    render() {
        return (
            <>
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.modalVisible}
                    onRequestClose={() => this.props.setModalVisible(false)}
                >
                    <TouchableOpacity
                        style={modalStyle.modalContainer}
                        activeOpacity={1}
                        onPressOut={() => this.props.setModalVisible(false)}
                    >

                        <TouchableWithoutFeedback>
                            <View style={modalStyle.modalContainerMain}>

                                {/* Large icon */}
                                <TouchableOpacity style={modalStyle.modalCardContainer}
                                    onPress={() => { this.props.setModalVisible(false); this.props.showTransactionPage(DEPOSIT); }}
                                >
                                    <Image
                                        resizeMode='contain'
                                        source={require("../assets/img/deposit_menu.png")}
                                        style={modalStyle.modalCardIcon}
                                    />
                                    <Text style={modalStyle.modalCardText}>Deposit</Text>
                                </TouchableOpacity>

                                <TouchableOpacity style={modalStyle.modalCardContainer}
                                    onPress={() => { this.props.setModalVisible(false); this.props.showTransactionPage(WITHDRAW) }}
                                >
                                    <Image
                                        resizeMode='contain'
                                        source={require("../assets/img/withdraw_menu.png")}
                                        style={modalStyle.modalCardIcon}
                                    />
                                    <Text style={modalStyle.modalCardText}>Reward</Text>
                                </TouchableOpacity>

                                <TouchableOpacity style={modalStyle.modalCardContainer} onPress={() => { this.props.setModalVisible(false); }}>
                                    <Image
                                        resizeMode='contain'
                                        source={require("../assets/img/rewards_menu.png")}
                                        style={modalStyle.modalCardIcon}
                                    />
                                    <Text style={modalStyle.modalCardText}>Reward Meta</Text>
                                </TouchableOpacity>

                                {/* Small icon */}
                                <View style={modalStyle.smallButtonWrapper}>
                                    <TouchableOpacity style={modalStyle.modalCardContainerSmall}
                                        onPress={() => this.showGameList("cuddle")}
                                    >
                                        <Image
                                            resizeMode='contain'
                                            source={require("../assets/img/cuddle.png")}
                                            style={modalStyle.modalCardIcon}
                                        />
                                        <Text style={modalStyle.modalCardText}>Cuddle Cup</Text>
                                    </TouchableOpacity>

                                    <TouchableOpacity style={modalStyle.modalCardContainerSmall}
                                        onPress={() => this.showGameList("cuddle")}
                                    >
                                        <Image
                                            resizeMode='contain'
                                            source={require("../assets/img/cuddle.png")}
                                            style={modalStyle.modalCardIcon}
                                        />
                                        <Text style={modalStyle.modalCardText}>Cuddle Cup Again</Text>
                                    </TouchableOpacity>

                                    <TouchableOpacity style={modalStyle.modalCardContainerSmall}
                                        onPress={() => this.showGameList("cappu")}
                                    >
                                        <Image
                                            resizeMode='contain'
                                            source={require("../assets/img/cappu.png")}
                                            style={modalStyle.modalCardIcon}
                                        />
                                        <Text style={modalStyle.modalCardText}>cappuchino</Text>
                                    </TouchableOpacity>

                                    <TouchableOpacity style={modalStyle.modalCardContainerSmall}
                                        onPress={() => this.showGameList("greenTea")}
                                    >
                                        <Image
                                            resizeMode='contain'
                                            source={require("../assets/img/green-tea.png")}
                                            style={modalStyle.modalCardIcon}
                                        />
                                        <Text style={modalStyle.modalCardText}>green tea</Text>
                                    </TouchableOpacity>

                                    <TouchableOpacity style={modalStyle.modalCardContainerSmall}
                                        onPress={() => this.showGameList("chooseCup")}
                                    >
                                        <Image
                                            resizeMode='contain'
                                            source={require("../assets/img/cuphidden.png")}
                                            style={modalStyle.modalCardIcon}
                                        />
                                        <Text style={modalStyle.modalCardText}>Choose cup?</Text>
                                    </TouchableOpacity>
                                </View>


                            </View>
                        </TouchableWithoutFeedback>

                    </TouchableOpacity>
                </Modal>

            </>
        )
    }

}

const modalStyle = StyleSheet.create({
    modalContainer: {
        // borderWidth: 2,
        // backgroundColor: 'white',
        height: '100%',
    },
    modalContainerMain: {
        marginTop: '20%',
        width: '100%',
        height: 300,
        flexWrap: 'wrap',
        flexDirection: 'row',
        alignSelf: 'center',
        alignContent: 'center',
        justifyContent: 'space-evenly',
        borderWidth: 1,
        borderColor: '#426177',
        backgroundColor: '#223041',
    },
    modalCardContainer: {
        width: 100,
        height: 100,
        // alignSelf: 'center',
        justifyContent: 'center',
        alignContent: 'center',
        borderWidth: 1,
        borderColor: '#426177',
        borderRadius: 5,
    },
    modalCardContainerSmall: {
        width: 60,
        height: 90,
        margin: 5,
        // alignSelf: 'center',
        justifyContent: 'center',
        alignContent: 'center',
        borderWidth: 1,
        borderColor: '#426177',
        borderRadius: 5,
    },
    modalCardIcon: {
        width: 40,
        height: 40,
        alignSelf: 'center',
        alignContent: 'center',
        justifyContent: 'center',
    },
    modalCardText: {
        marginTop: 10,
        color: 'white',
        alignSelf: 'center',
        alignContent: 'center',
        justifyContent: 'center',
    },
    smallButtonWrapper: {
        // borderWidth: 1,
        marginTop: 5,
        width: '100%',
        // flexWrap: 'wrap',
        flexDirection: 'row',
        // alignSelf: 'center',
        // alignContent: 'center',
        justifyContent: 'space-evenly',
    },
});

