import React, { Component } from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import Swiper from 'react-native-swiper'

const styles = StyleSheet.create({
    headerContainer: {
        flex: 1,
        justifyContent: "center",
        alignSelf: "center",
    },
    imageSlide: {
        width: 250,
        alignSelf: "center",
    },
    slide1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
    },
    slide2: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    slide3: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
});

export default class ImageSlide extends Component {
    render() {
        return (
            <>
                <View style={styles.headerContainer}>
                    <Swiper style={styles.wrapper} showsButtons={true} showsPagination={false} autoplay={true}>
                        <View style={styles.slide2}>
                            <Image
                                resizeMode="contain"
                                source={require("../assets/img/cuddle.png")}
                                style={styles.imageSlide}
                            />
                        </View>
                        <View style={styles.slide3}>
                            <Image
                                resizeMode="contain"
                                source={require("../assets/img/cappu.png")}
                                style={styles.imageSlide}
                            />
                        </View>
                        <View style={styles.slide3}>
                            <Image
                                resizeMode="contain"
                                source={require("../assets/img/green-tea.png")}
                                style={styles.imageSlide}
                            />
                        </View>
                    </Swiper>
                </View>
            </>
        )
    }
}

