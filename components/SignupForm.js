import React, { useCallback } from 'react';
import { Linking, View, Text, StyleSheet, TextInput, Image, TouchableOpacity } from "react-native"
import { Formik } from 'formik';
import * as Yup from 'yup';
import ModalDropdown from 'react-native-modal-dropdown';
import { AntDesign } from '@expo/vector-icons';
import { LinearGradient } from 'expo-linear-gradient';

require('../global');

const OPTIONS =
    [
        "SCB ธนาคารไทยพาณิชย์",
        "KBANK ธนาคารกสิกรไทย",
        "KTB ธนาคารกรุงไทย",
        "BBL ธนาคารกรุงเทพ",
        "BAY ธนาคารกรุงศรี",
        "TBANK ธนาคารธนชาต",
        "TMB ธนาคารทหารไทย",
        "GSB ธนาคารออมสิน",
        "BAG ธกส.",
        "UOB ธนาคารยูโอบี",
        "CIMB ธนาคารซีไอเอ็มบี",
        "TTTB ธนาคารทหารไทยธนชาต"
    ]

const lineNazaURL = global.lineUrl;

const OpenURLButton = ({ url }) => {
    const handlePress = useCallback(async () => {
        const supported = await Linking.canOpenURL(url);

        if (supported) {
            await Linking.openURL(url);
        } else {
            Alert.alert(`Link is not available: ${url}`);
        }
    }, [url]);

    return (
        <TouchableOpacity style={acknowledgeStyle.lineContainer} onPress={handlePress}>
            <Image style={acknowledgeStyle.lineButton}
                mode='contain'
                source={require('../assets/img/line-click.gif')}
            />
        </TouchableOpacity >
    )
};

const SignupForm = React.forwardRef((props, ref) => {
    const phoneRegExp = /^0[0-9]{9}$/

    const OtpSchema = Yup.object().shape({

        phoneNumber: Yup.string()
            .min(10, 'Wrong number!')
            .max(10, 'Wrong number!')
            .matches(phoneRegExp, 'Wrong number!')
            .required('Wrong number!'),
    });

    const RegisterSchema = Yup.object().shape({

        firstname: Yup.string()
            .min(2, 'Incorrect value!')
            .max(50, 'Exceed limit')
            .required('Fill me meh!'),

        lastname: Yup.string()
            .min(2, 'Incorrect value!')
            .max(50, 'Exceed limit')
            .required('Fill me meh!'),

        nickname: Yup.string()
            .min(2, 'Incorrect value!')
            .max(20, 'Too much!')
            .required('Fill me meh!'),

        bankNo: Yup.string()
            .min(10, 'At least 10 digit')
            .max(10, 'At least 10 digit')
            .required('At least 10 digit'),

        phoneno: Yup.string()
            .min(10, 'At least 10 digit')
            .max(10, 'At least 10 digit')
            .required('At least 10 digit'),


    });

    if (props.type === "acknowledge") {
        return (
            <LinearGradient
                colors={['rgba(78,140,141,1)', 'rgba(192,240,155,1)']}
                style={acknowledgeStyle.cardContainer}
            >
                <View>
                    <Text style={acknowledgeStyle.cardTitle}>
                        Registered
                </Text>

                    <Text style={acknowledgeStyle.subText}>
                        100% completed!
                    </Text>

                    <View
                        style={{
                            borderBottomColor: 'black',
                            borderBottomWidth: 1,
                            margin: 25,
                        }}
                    />

                    <OpenURLButton url={props.lineUrl != undefined ? props.lineUrl : lineNazaURL} />

                </View>
            </LinearGradient>
        )
    }

    else if (props.type === "otp") {
        return (
            <Formik
                initialValues={{
                    phoneNumber: ''
                }}
                onSubmit={values => {
                    props.updatePhoneNumberOTP(values.phoneNumber)
                }}
                validationSchema={OtpSchema}
            >
                {({ values, handleChange, errors, setFieldTouched, touched, isValid, handleSubmit }) => (
                    <View style={phoneRegisterStyle.container}>
                        <Text style={phoneRegisterStyle.title}>Register</Text>
                        <TextInput
                            value={values.phoneNumber}
                            onChangeText={handleChange('phoneNumber')}
                            onBlur={() => setFieldTouched('phoneNumber')}
                            placeholder="phoneNumber"
                            style={phoneRegisterStyle.input}
                            placeholder="Ex: 0890000000"
                            placeholderTextColor="grey"
                            keyboardType="numeric"
                        />
                        {touched.phoneNumber && errors.phoneNumber &&
                            <Text style={{ fontSize: 12, color: '#FF0D10' }}>{errors.phoneNumber}</Text>
                        }
                        <TouchableOpacity style={phoneRegisterStyle.otp} onPress={handleSubmit}>
                            <Text style={phoneRegisterStyle.submitText}>ขอรับ OTP</Text>
                        </TouchableOpacity>
                    </View>
                )}
            </Formik>
        )
    }
    else if (props.type === "confirmOTP") {
        return (
            <View style={phoneRegisterStyle.container}>
                <Text style={phoneRegisterStyle.titleOtpSent}>ระบบได้ส่ง OTP ไปทาง SMS เบอร์มือถือของท่าน{"\n"}กรุณากรอกรหัสที่ได้รับเพื่อยืนยันตัวตน</Text>

                <Text style={phoneRegisterStyle.text}>เบอร์มือถือ: {props.phoneNumberOtp}</Text>
                <Text style={phoneRegisterStyle.text}>REF : {props.otpRef}</Text>

                <TextInput
                    style={phoneRegisterStyle.input}
                    onChangeText={(val) => props.checkOtpOnType(val)}
                    placeholder="รหัส OTP"
                    placeholderTextColor="grey"
                    keyboardType="numeric"
                />
                {props.otpPressed && props.otpInvalid &&
                    <Text style={{ fontSize: 12, color: '#FF0D10' }}>รหัส OTP ไม่ถูกต้อง จำนวนครั้งที่เหลือ {props.otpCountLeft} ครั้ง
                        </Text>
                }

                {props.otpCountLeft === 0 && props.otpTimeCount > 1 &&
                    <Text style={{ fontSize: 12, color: '#FF0D10' }}>ขอรับรหัสใหม่อีกครั้งได้ใน {props.otpTimeCount} วินาที
                        </Text>
                }

                {props.otpExpired &&
                    < Text style={{ fontSize: 12, color: '#FF0D10' }}>รหัส OTP หมดอายุแล้ว กรุณากดรับรหัสใหม่อีกครั้ง</Text>
                }

                {
                    (props.showNewRequestOtpButton == true && props.otpExpired === true) &&
                    <TouchableOpacity style={phoneRegisterStyle.otp} onPress={props.resentOtpRequest} disabled={props.resentOtpPressed}>
                        <Text style={props.resentOtpPressed > 0 ? phoneRegisterStyle.submitTextDisable : phoneRegisterStyle.submitText}>รับรหัส OTP ใหม่</Text>
                    </TouchableOpacity>
                }

                <TouchableOpacity style={phoneRegisterStyle.otp} onPress={props.verifyOtp} disabled={props.otpCountLeft === 0}>
                    <Text style={props.otpCountLeft === 0 ? phoneRegisterStyle.submitTextDisable : phoneRegisterStyle.submitText}>ยืนยันรหัส OTP</Text>
                </TouchableOpacity>

            </View >
        )
    }
    else if (props.type === "signup") {
        return (
            <Formik
                initialValues={{
                    firstname: '',
                    lastname: '',
                    nickname: '',
                    bankNo: '',
                    bankName: '',
                    phoneno: '',
                    email: props.email,
                    sid: props.sid
                }}
                onSubmit={values => {
                    if (props.bankName == '') {
                        return false
                    }
                    props.submitForm(values)
                }}
                validationSchema={RegisterSchema}
            >
                {({ values, handleChange, errors, setFieldTouched, touched, isValid, handleSubmit }) => (
                    <View style={phoneRegisterStyle.container}>
                        <TextInput
                            style={phoneRegisterStyle.input}
                            placeholder="Name *"
                            placeholderTextColor="grey"
                            autoFocus={true}

                            value={values.firstname}
                            onChangeText={handleChange('firstname')}
                            onBlur={() => setFieldTouched('firstname')}

                        />
                        {touched.firstname && errors.firstname &&
                            <Text style={{ fontSize: 12, color: '#FF0D10' }}>{errors.firstname}</Text>
                        }

                        <TextInput
                            style={phoneRegisterStyle.input}
                            placeholder="Surname *"
                            placeholderTextColor="grey"

                            value={values.lastname}
                            onChangeText={handleChange('lastname')}
                            onBlur={() => setFieldTouched('lastname')}
                        />

                        {touched.lastname && errors.lastname &&
                            <Text style={{ fontSize: 12, color: '#FF0D10' }}>{errors.lastname}</Text>
                        }

                        <TextInput
                            style={phoneRegisterStyle.input}
                            placeholder="Nickname *"
                            placeholderTextColor="grey"

                            value={values.nickname}
                            onChangeText={handleChange('nickname')}
                            onBlur={() => setFieldTouched('nickname')}
                        />

                        {touched.nickname && errors.nickname &&
                            <Text style={{ fontSize: 12, color: '#FF0D10' }}>{errors.nickname}</Text>
                        }

                        <TextInput
                            style={phoneRegisterStyle.input}
                            placeholder="wallet address *"
                            placeholderTextColor="grey"
                            keyboardType="numeric"

                            value={values.bankNo}
                            onChangeText={handleChange('bankNo')}
                            onBlur={() => setFieldTouched('bankNo')}
                        />

                        {touched.bankNo && errors.bankNo &&
                            <Text style={{ fontSize: 12, color: '#FF0D10' }}>{errors.bankNo}</Text>
                        }

                        <TextInput
                            style={phoneRegisterStyle.input}
                            placeholder="Phone *"
                            placeholderTextColor="grey"
                            keyboardType="numeric"

                            value={values.phoneno}
                            onChangeText={handleChange('phoneno')}
                            onBlur={() => setFieldTouched('phoneno')}
                        />

                        {touched.phoneno && errors.phoneno &&
                            <Text style={{ fontSize: 12, color: '#FF0D10' }}>{errors.phoneno}</Text>
                        }

                        <ModalDropdown
                            ref={ref}
                            style={phoneRegisterStyle.dropdownContainer}
                            adjustFrame={style => props._adjustFrame(style)}
                            renderRow={props._dropdownRenderRow}
                            options={OPTIONS}>

                            <View style={phoneRegisterStyle.selectedBank}>
                                <Image style={props.bankName !== '' ? phoneRegisterStyle.selectedBankIcon : { width: 0 }}
                                    mode='contain'
                                    source={props.bankIconSource === null ? null : props.bankIconSource}
                                />
                                <Text style={phoneRegisterStyle.dropdownTitle}>
                                    {props.bankName !== '' ? props.bankName : 'ธนาคาร'}
                                </Text>

                                <View style={{ justifyContent: 'center', marginLeft: 'auto', marginRight: 10 }} >
                                    <AntDesign name="down" size={20} color="white" />
                                </View>

                            </View>

                        </ModalDropdown>

                        {touched.bankName && props.bankName == '' &&
                            <Text style={{ fontSize: 12, color: '#FF0D10' }}>Select bank</Text>
                        }

                        <TouchableOpacity style={phoneRegisterStyle.otp} onPress={handleSubmit}>
                            <Text style={phoneRegisterStyle.submitText}>ยืนยันข้อมูล</Text>
                        </TouchableOpacity>

                    </View>
                )}
            </Formik>
        )
    }

});

export default SignupForm;

const acknowledgeStyle = StyleSheet.create({
    cardContainer: {
        alignContent: 'center',
        justifyContent: 'center',
        margin: 12,
        marginTop: 200,
        borderRadius: 32,
        height: 400,
        // backgroundColor: 'white',
        elevation: 30,
        // borderWidth: 5,
    },
    cardTitle: {
        color: 'white',
        marginTop: 25,
        fontSize: 20,
        alignSelf: 'center'
    },
    subText: {
        color: 'white',
        marginTop: 25,
        fontSize: 12,
        alignSelf: 'center',
        flexWrap: 'wrap',
        alignItems: 'stretch',
    },
    lineContainer: {
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center',
        flexWrap: 'wrap',
        width: '50%',
        // borderWidth: 2,
    },
    lineButton: {
        justifyContent: 'center',
        width: '100%',
        height: '50%'
    },
})

const phoneRegisterStyle = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 45,
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: 'white',
        // borderWidth: 5
    },
    title: {
        margin: 12,
        alignSelf: 'flex-start',
        fontSize: 15,
        color: 'white',

    },
    submitText: {
        margin: 12,
        alignSelf: 'center',
        fontSize: 19,
        color: 'white',
    },
    submitTextDisable: {
        margin: 12,
        alignSelf: 'center',
        fontSize: 19,
        color: 'grey',
    },
    input: {
        margin: 12,
        alignSelf: 'flex-start',
        width: '90%',
        borderRadius: 2,
        borderBottomWidth: 0.2,
        color: 'white',
        borderColor: 'white',
        justifyContent: 'center',
        paddingHorizontal: 10
    },
    titleOtpSent: {
        margin: 12,
        marginBottom: 10,
        textAlign: 'center',
        fontFamily: 'sukhumvitset-thin-webfont',
        // lineHeight: 15,
        fontSize: 15,
        color: 'white',
    },
    text: {
        fontSize: 15,
        margin: 12,
        alignSelf: 'flex-start',
        color: 'white'
    },
    otp: {
        margin: 12,
        marginTop: 40,
        width: '95%',
        height: 50,
        borderWidth: 2,
        borderRadius: 5,
        borderColor: '#426177',
        justifyContent: 'center',
    },
    dropdownContainer: {
        margin: 12,
        height: 50,
        alignSelf: 'flex-start',
        width: '90%',
        color: 'white',
        borderColor: 'white',
        borderWidth: 1,
    },
    bankDropdown: {
        margin: 12,
        flexDirection: 'row',
        height: 50,
        justifyContent: 'flex-start',
        flexWrap: 'wrap',
        // borderWidth: 10,
    },
    dropdownTitle: {
        fontSize: 15,
        margin: 12,
        color: 'white',
    },
    dropdownText: {
        fontSize: 12,
        margin: 15,
    },
    dropdownImage: {
        width: '15%',
        height: '100%',
    },
    selectedBank: {
        flexDirection: 'row',
    },
    selectedBankIcon: {
        margin: 4,
        width: '15%',
        height: '90%',
        alignSelf: 'flex-start',
    }
})