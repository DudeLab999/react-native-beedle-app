import React, { Component } from "react";
import { LinearGradient } from 'expo-linear-gradient';
import { Table, Row} from 'react-native-table-component';
import {
    View, Text, StyleSheet, Linking, 
    Image, TouchableOpacity
} from "react-native";

import axios from 'axios'

class Landing extends Component {
    constructor(props) {
        super(props);

        this.state = {
            ...props,
            isShowProfile: false,
            showModal: true
        };
    }

    showPromotion = () => {
        let url = `https://iM.Academy/corp/cjoin`;

        const supported = Linking.canOpenURL(url);

        if (supported) {
            Linking.openURL(url);
        } else {
            Alert.alert(`ไม่สามารถเปิด link ได้ : ${url}`);
        }
    }

    toggleProfile = () => {
        this.setState(p => ({ isShowProfile: !p.isShowProfile }))
    }


    logout = () => {
        this.props.navigation.reset({ index: 0, routes: [{ name: 'Home' }], });
    }

    refresh = () => {
        this.setState(p => ({ isShowProfile: false }))
    }

    closeModal = () => {
        this.setState(p => ({ showModal: !p.showModal }))
    }

    render() {
        return (
            <>
                <View style={mainStyle.container}>
                    {!this.state.isShowProfile &&
                        <View style={{ flex: 1 }}>
                            {/* Modal */}
                            {this.state.showModal &&
                                <TouchableOpacity style={{
                                    zIndex: 10,
                                    marginTop: '10%',
                                    flex: 1, height: '110%', backgroundColor: "rgba(0,0,0,0.7)", position: 'absolute', top: 0, left: 0,
                                    bottom: 0, right: 0
                                }}
                                    onPress={() => this.closeModal()}
                                >
                                    <Text style={{ marginTop: '60%', alignSelf: 'center', justifyContent: 'center', color: '#ffffff', fontSize: 26 }}>{"Creat your own business today."}</Text>
                                    <Image
                                        style={{ marginTop: -250, flex: 1, height: '100%' }}
                                        source={{ uri: "https://bestlifetimeincome.com/wp-content/uploads/2020/04/im-mastery-academy-review.png" }}
                                        resizeMode="contain"
                                    />

                                </TouchableOpacity>
                            }
                            {/* Content */}
                            <View style={{ flex: .5, borderWidth: 0, marginTop: '5%' }}>
                                <Text style={{ alignSelf: 'center', justifyContent: 'center' }}>{this.state.nickname}</Text>
                                <Text style={{ alignSelf: 'center', justifyContent: 'center', color: '#ffffff', textAlign: 'center', fontSize: 12 }}>{"Please note that: You're free member the indicator will not upload \n to your app directly"}</Text>

                                <View style={{ height: '80%', flexDirection: 'column', marginTop: 15 }}>
                                    <Image
                                        style={{ flex: 1, height: '100%' }}
                                        source={{ uri: "https://im.academy/images/backoffice2019/03_CTmarketmastery.jpg" }}
                                        resizeMode="contain"
                                    />
                                    <Text style={{ alignSelf: 'center', justifyContent: 'center', color: '#ffffff' }}>
                                        {"TOP GOLIVE EDUCATORS"}
                                    </Text>
                                </View>

                            </View>
                            <View style={{ borderWidth: 0, flex: .5, marginTop: '10%', height: '100%', alignSelf: 'center', justifyContent: 'center' }}>
                                <Text style={{ alignSelf: 'center', justifyContent: 'center', color: "#ffffff", fontSize: 10 }}>{"Indicator Alert Example"}</Text>
                                <Image
                                    style={{ flex: 1, height: '40%' }}
                                    source={require("../assets/img/package-ex.png")}
                                    resizeMode="contain"
                                />
                                <Text style={{ alignSelf: 'center', justifyContent: 'center', color: "#ffffff" }}>{"Upgrade subscription plan for realtime strategies"}</Text>
                                <Text onPress={() => { this.showPromotion() }} style={{ alignSelf: 'center', justifyContent: 'center', fontSize: 20, color: "#ffffff", marginTop: 20, fontWeight: '700', textDecorationLine: 'underline', color: '#FFC400' }}>{"Upgrade Plan"}</Text>
                            </View>
                        </View>
                    }

                    {this.state.isShowProfile && <View style={this.state.isShowProfile ? profileStlye.container : profileStlye.hide}>
                        <View style={profileStlye.title}>
                            <Image
                                style={profileStlye.image}
                                source={require("../assets/img/account_bt.png")}
                                resizeMode="contain"
                            />
                            <Text style={profileStlye.titleText}>Profile</Text>
                        </View>

                        <View style={{ width: '100%' }}>
                            <Table borderStyle={{ borderWidth: 1, borderColor: '#C1C0B9' }}>
                                <Row data={['Name', <Text style={profileStlye.text}>{this.state.initial.firstname}</Text>]} style={profileStlye.headerTable} textStyle={{ color: 'white', margin: 10 }} />
                                <Row data={['Surname', <Text style={profileStlye.text}>{this.state.initial.lastname}</Text>]} style={profileStlye.headerTable} textStyle={{ color: 'white', margin: 10 }} />
                                <Row data={['Nickname', <Text style={profileStlye.text}>{this.state.initial.nickname}</Text>]} style={profileStlye.headerTable} textStyle={{ color: 'white', margin: 10 }} />
                                <Row data={['Username', <Text style={profileStlye.text}>{this.state.initial.username}</Text>]} style={profileStlye.headerTable} textStyle={{ color: 'white', margin: 10 }} />
                                <Row data={['Account No.', <Text style={profileStlye.text}>{this.state.initial.bankNo}</Text>]} style={profileStlye.headerTable} textStyle={{ color: 'white', margin: 10 }} />
                                <Row data={['Registered Bank', <Text style={profileStlye.text}>{this.state.initial.bankName}</Text>]} style={profileStlye.headerTable} textStyle={{ color: 'white', margin: 10 }} />
                                <Row data={['Mobile', <Text style={profileStlye.text}>{this.state.initial.phone}</Text>]} style={profileStlye.headerTable} textStyle={{ color: 'white', margin: 10 }} />
                                <Row data={['Current Package', <Text style={profileStlye.text}>{"Free Member"}</Text>]} style={profileStlye.headerTable} textStyle={{ color: 'white', margin: 10 }} />
                            </Table>
                        </View>

                        <View style={profileStlye.buttonWrapper}>
                            <LinearGradient style={profileStlye.button}
                                colors={['#223041', '#212529', '#426177']}
                                start={[1, 0]}
                                end={[1, 1]}
                            >
                                <TouchableOpacity style={{ alignSelf: 'center' }} onPress={() => this.logout()}>
                                    <Text style={profileStlye.buttonText}>Logout</Text>
                                </TouchableOpacity>
                            </LinearGradient>
                        </View>

                    </View>}

                    {/* Footer */}
                    <View style={MainStyle.footer}>
                        <TouchableOpacity style={MainStyle.footerButton}
                            onPress={() => this.props.navigation.navigate("Main")}
                        >
                            <Image
                                resizeMode='contain'
                                source={require("../assets/img/home_bottom_icon.png")}
                                style={MainStyle.footerButtonImage}
                            />
                            <Text style={MainStyle.footerIcon}>Home</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.showPromotion()}
                            style={MainStyle.footerButton}>
                            <Image
                                resizeMode='contain'
                                source={require("../assets/img/promotion_bottom_icon.png")}
                                style={MainStyle.footerButtonImage}
                            />
                            <Text style={MainStyle.footerIcon}>Promotion</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={MainStyle.footerMain}
                            onPress={() => this.refresh()}
                        >
                            <Image
                                resizeMode='contain'
                                source={require("../assets/img/cup_bt.png")}
                                style={MainStyle.footerMainButtonImage}
                            />
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => this.toggleProfile()}
                            style={MainStyle.footerButton}>
                            <Image
                                resizeMode='contain'
                                source={require("../assets/img/member_icon_gold.png")}
                                style={MainStyle.footerButtonImage}
                            />
                            <Text style={MainStyle.footerIcon}>profile</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={MainStyle.footerButton}
                            onPress={() => this.showPromotion()}
                        // onPress={() => this.props.navigation().navigate("Chat")}
                        >
                            <Image
                                resizeMode='contain'
                                source={require("../assets/img/contact_bottom_bt.png")}
                                style={MainStyle.footerButtonImage}
                            />
                            <Text style={MainStyle.footerIcon}>Upline</Text>
                        </TouchableOpacity>
                    </View>


                    <LinearGradient style={MainStyle.footerBackground}
                        colors={['#223041', '#212529', '#426177']}
                        start={[1, 0]}
                        end={[1, 1]}
                    >
                    </LinearGradient>

                </View>
            </>
        )
    }

}

const mainStyle = StyleSheet.create({
    container: {
        // borderWidth: 10,
        flex: 1,
        width: '100%',
        // justifyContent: 'center'
    }
});

const MainStyle = StyleSheet.create({
    mainContainer: {
        flex: 1,
        flexDirection: 'column'
    },
    loading: {
        position: 'absolute',
        alignSelf: 'center',
        alignItems: 'center',
        zIndex: 100,
        width: '100%',
        height: '100%',
        borderRadius: 10,
        backgroundColor: 'rgba(255,255,255,0.9)',
        justifyContent: 'center',

    },
    loadingText: {
        fontSize: 49
    },
    headerNav: {
        flex: .11,
        width: '100%',
        justifyContent: 'space-between',
        flexDirection: 'row',
        // borderWidth: 1,
        borderColor: 'white',
    },
    profileContainer: {
        marginLeft: 15,
        // borderWidth: 1,
        borderColor: 'white',
        justifyContent: 'center',
    },
    userProfile: {
        width: 35,
        height: 35,
        // borderWidth: 1,
        alignSelf: 'center',
        justifyContent: 'center',
    },
    usernameText: {
        fontSize: 9,
        color: 'white',
        alignSelf: 'center',
        alignContent: 'center',
        justifyContent: 'center',
    },
    mainPageLogoContainer: {
        alignContent: 'center',
        justifyContent: 'center',
        // borderWidth: 1,
        borderColor: 'white',
    },
    mainPageLogo: {
        width: 150,
        alignContent: 'center',
        justifyContent: 'center',
    },
    hamburgerMenu: {
        height: '100%',
        marginRight: '3%',
        alignSelf: 'flex-end',
        alignContent: 'center',
        justifyContent: 'center',
        // borderWidth: 1,
        borderColor: 'white',
    },
    hamburgerIcon: {
        width: 50,
        alignSelf: 'center',
        alignContent: 'center',
        justifyContent: 'center',
    },
    container: {
        flex: .9,
        marginTop: 45,
        alignItems: 'center',
        // borderWidth: 1,
        borderColor: 'green',
    },
    footerBackground: {
        position: 'absolute',
        bottom: 0,
        borderColor: '#426177',
        borderWidth: 1,
        width: '100%',
        height: 65,
        zIndex: -10,
    },
    footerMainButtonImage: {
        width: '100%',
        alignSelf: "center",
        justifyContent: 'center',
        height: '100%'
    },
    footerButtonImage: {
        width: 30,
        alignSelf: "center",
        justifyContent: 'center',
        // borderWidth: 1,
        height: 30
    },
    footer: {
        flex: .14,
        flexDirection: 'row',
        // borderWidth: 5,
        // flexWrap: 'wrap',
        // alignSelf: 'flex-end',
        justifyContent: 'space-between',
        alignContent: 'center',
        // borderWidth: 2,
        borderColor: 'white',
    },
    footerButton: {
        height: 70,
        width: '18%',
        borderRadius: 15,
        flexDirection: 'column',
        alignSelf: 'flex-end',
        alignContent: 'center',
        justifyContent: 'center',
        // borderWidth: 1,
        borderColor: 'white',
    },
    footerIcon: {
        fontSize: 10,
        color: 'white',
        justifyContent: 'center',
        alignSelf: 'center',
    },
    footerMain: {
        height: 90,
        width: '22%',
        borderRadius: 360,
        marginTop: 10,
        alignContent: 'center',
        justifyContent: 'center',
        borderColor: 'white',
        zIndex: 1,
    },
});


const profileStlye = StyleSheet.create({
    container: {
        // borderWidth: 1,
        flex: 1,
        marginTop: '10%',
        width: '90%',
        marginLeft: '5%'
    },
    hide: {
        flex: 0,
        display: 'none'
    },
    title: {
        // borderWidth: 1,
        flex: .2,
        // width: '100%',
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignSelf: 'center',
    },
    image: {
        // borderWidth: 1,
        height: '100%',
        width: '10%',
        alignSelf: 'center',
        justifyContent: 'center',
        alignContent: 'center',
    },
    titleText: {
        fontSize: 24,
        textAlign: 'center',
        fontFamily: 'sukhumvitset-thin-webfont',
        color: 'white',
        fontWeight: '600',
        marginLeft: 5,
        alignSelf: 'center',
        justifyContent: 'center',
        alignContent: 'center',
    },
    text: {
        fontSize: 14,
        color: 'white',
        fontWeight: '500',
        textAlign: 'right',
        fontFamily: 'sukhumvitset-thin-webfont',
        margin: 10,
    },
    headerTable: {
        justifyContent: 'center',
        alignContent: 'center',
        height: 40,
        backgroundColor: '#181818',
    },
    buttonWrapper: {
        flex: .3,
        marginTop: '10%',
        justifyContent: 'center',
        alignContent: 'center',
    },
    button: {
        width: '60%',
        height: '50%',
        marginTop: '5%',
        borderRadius: 5,
        alignSelf: 'center',
        justifyContent: 'center',
        alignContent: 'center',
    },
    buttonText: {
        fontSize: 18,
        color: 'white',
        fontWeight: '600',
        textAlign: 'center',
        fontFamily: 'sukhumvitset-thin-webfont',
        justifyContent: 'center',
        alignContent: 'center',
    },

});

export default Landing;