import React, { Component } from 'react';
import {
    View, Text, StyleSheet,
    Image, TouchableOpacity
} from "react-native";
require('../global.js');
import HeaderModal from '../components/HeaderModal';

export default class HeaderMain extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: this.props.username
        }
    }

    setModalVisible = (status) => {
        this.setState(
            (
                {
                    modalVisible: status
                }
            )
        );
    }

    showTransactionPage = (page) => {
        this.setState(
            (
                {
                    modalVisible: false,
                    selectedPaymentPage: page,
                    transactionPageActive: page,
                    test: "Ok"
                }
            )
        );
        this.props.navigation.navigate("Transaction", { ...this.state, transactionPageActive: page });
    }

    render() {

        const headerModal = <HeaderModal setModalVisible={(status) => this.setModalVisible(status)} showTransactionPage={(page) => this.props.toTransactionPage(page)} />
        return (
            <View style={MainStyle.headerNav}>
                <View style={MainStyle.profileContainer}>
                    <Image
                        resizeMode='contain'
                        source={require("../assets/img/cup_bt.png")}
                        style={MainStyle.userProfile}
                    />
                    <Text style={MainStyle.usernameText}>New User</Text>
                    <Text style={MainStyle.usernameText}>{this.state.username}</Text>
                </View>

                <TouchableOpacity style={MainStyle.mainPageLogoContainer}
                    onPress={() => this.props.navigation.navigate("Main")}
                >
                    <Image
                        resizeMode='contain'
                        source={global.LOGO}
                        style={MainStyle.mainPageLogo}
                    />
                </TouchableOpacity>

                <TouchableOpacity style={MainStyle.hamburgerMenu}
                    onPress={() => this.setModalVisible(true)}
                >
                    <Image
                        resizeMode='contain'
                        source={require("../assets/img/right_top_menu.png")}
                        style={MainStyle.hamburgerIcon}
                    />

                </TouchableOpacity>
                {this.state.modalVisible && headerModal}

            </View>
        )
    }

}

const MainStyle = StyleSheet.create({
    mainContainer: {
        flex: 1,
    },
    loading: {
        position: 'absolute',
        alignSelf: 'center',
        alignItems: 'center',
        zIndex: 100,
        width: '100%',
        height: '100%',
        borderRadius: 10,
        backgroundColor: 'rgba(255,255,255,0.9)',
        justifyContent: 'center',

    },
    loadingText: {
        fontSize: 49
    },
    headerNav: {
        flex: .25,
        width: '100%',
        justifyContent: 'space-between',
        flexDirection: 'row',
        // borderWidth: 1,
        borderColor: 'white',
    },
    profileContainer: {
        marginLeft: 15,
        // borderWidth: 1,
        borderColor: 'white',
        justifyContent: 'center',
    },
    userProfile: {
        width: 35,
        height: 35,
        // borderWidth: 1,
        alignSelf: 'center',
        justifyContent: 'center',
    },
    usernameText: {
        fontFamily: 'sukhumvitset-text-webfont',
        textAlign: 'center',
        fontSize: 9,
        color: 'white',
        alignSelf: 'center',
        alignContent: 'center',
        justifyContent: 'center',
    },
    mainPageLogoContainer: {
        alignContent: 'center',
        justifyContent: 'center',
        // borderWidth: 1,
        borderColor: 'white',
    },
    mainPageLogo: {
        width: 150,
        alignContent: 'center',
        justifyContent: 'center',
    },
    hamburgerMenu: {
        height: '100%',
        marginRight: '3%',
        alignSelf: 'flex-end',
        alignContent: 'center',
        justifyContent: 'center',
        // borderWidth: 1,
        borderColor: 'white',
    },
    hamburgerIcon: {
        width: 50,
        alignSelf: 'center',
        alignContent: 'center',
        justifyContent: 'center',
    },
    container: {
        flex: .9,
        marginTop: 45,
        alignItems: 'center',
        // borderWidth: 1,
        borderColor: 'green',
    },
    footerBackground: {
        position: 'absolute',
        bottom: 0,
        borderColor: '#426177',
        borderWidth: 1,
        width: '100%',
        height: 65,
        zIndex: -10,
    },
    footerMainButtonImage: {
        width: '100%',
        alignSelf: "center",
        justifyContent: 'center',
        height: '100%'
    },
    footerButtonImage: {
        width: 30,
        alignSelf: "center",
        justifyContent: 'center',
        // borderWidth: 1,
        height: 30
    },
    footer: {
        flex: .15,
        flexDirection: 'row',
        // flexWrap: 'wrap',
        justifyContent: 'space-between',
        alignContent: 'center',
        // borderWidth: 2,
        borderColor: 'white',
    },
    footerButton: {
        height: 70,
        width: '18%',
        borderRadius: 15,
        flexDirection: 'column',
        alignSelf: 'flex-end',
        alignContent: 'center',
        justifyContent: 'center',
        // borderWidth: 1,
        borderColor: 'white',
    },
    footerIcon: {
        fontSize: 10,
        color: 'white',
        justifyContent: 'center',
        alignSelf: 'center',
    },
    footerMain: {
        height: 90,
        width: '22%',
        borderRadius: 360,
        marginTop: 10,
        alignContent: 'center',
        justifyContent: 'center',
        borderColor: 'white',
        zIndex: 1,
    },
});


