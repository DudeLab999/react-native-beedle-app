import React, { Component } from 'react';
import {
    View, Text, StyleSheet,
    Image, TouchableOpacity, ScrollView, Alert, Linking
} from "react-native";

import { WebView } from 'react-native-webview';

import HeaderMain from '../components/HeaderMain';
import Footer from '../components/Footer';

import axios from 'axios'

require('../global.js');

const apiUrl = global.API_URL;

export default class PlayAccess extends Component {
    constructor(props) {
        super(props);
        // console.log(props)
        const auth = this.props.route.params;

        this.state = {
            token: auth.access_token,
            username: auth.username,
            ...this.props.route.params,

            modalVisible: false,
            loading: false,

            gameType: this.props.route.params.gameType,
            cupNames: [],
            cuddleNames: [],
            chooseCupCards: [],
            chooseCups: [],
            gameLabel: "",

            selectedGame: "",

            showMaincup: true,
            showSubcup: false,
            showcuddle: false,
            showchooseCup: false,
            showGameCard: false,
        }
    }

    setModalVisible = (status) => {
        this.setState(
            (
                {
                    modalVisible: status
                }
            )
        );
    }

    showMainPage = () => {
        this.props.navigation.navigate("Main", { ...this.state });
    }

    showTransactionPage = (page) => {
        this.setState(
            (
                {
                    modalVisible: false,
                    selectedPaymentPage: page,
                    transactionPageActive: page
                }
            )
        );
        this.props.navigation.navigate("Transaction", { ...this.state, transactionPageActive: page });
    }

    componentDidMount() {
        // console.log("mo")
        const config = {
            headers: { Authorization: `Bearer ${this.state.token}` }
        };
        console.log(config)

        // Get cups
        if (this.state.gameType == "cappu") {
            axios.get
                (
                    `${apiUrl}/getAvailablecup`,
                    config
                )
                .then((response) => {
                    if (response.status == 200) {
                        const result = response.data;
                        const cupOrder = global.cupOrder;

                        const cupMaps = result.cup_maps;

                        // Check for order sequence : [15,16] <- show id 15,16 first,second, other normal sequence
                        const apicupOrder = result.cup_order;

                        const cup_keys_values = result.cup_keys_values;

                        // console.log(result)

                        let orderedcup = [];

                        let tmp = [];
                        // console.log(apicupOrder);

                        // Add cup data to ordered cup
                        for (let i = 0; i < apicupOrder.length; i++) {
                            tmp.push(cup_keys_values[apicupOrder[i]][2]);
                            orderedcup.push(cup_keys_values[apicupOrder[i]]);
                        }

                        // Same as above but for remaining order [not priorities game]
                        for (let j = 0; j < cupMaps.length; j++) {
                            if (!tmp.includes(cupMaps[j][2])) {
                                orderedcup.push(cupMaps[j]);
                            }

                        }

                        this.setState(
                            (
                                {
                                    gameLabel: "Cup head",
                                    cupNames: orderedcup,
                                    cupMaps: cupMaps,
                                }
                            )
                        );

                    }
                })
                .catch((error) => {
                    console.log(error)
                    if (error.response) {
                        if (error.response.status == 401) {
                            Alert.alert(
                                `Session หมดอายุ กรุณาล็อกอินใหม่`,
                                `Session has expired, Please login`,
                            );
                        }
                        else if (error.response.status == 500) {
                            // console.log(error.response.status);
                            Alert.alert(
                                `Attention`,
                                `Oops! Try again next time`,
                            );
                        }
                        this.props.navigation.goBack();
                    }

                });
        }
        else if (this.state.gameType == "cuddle") {
            axios.get
                (
                    `${apiUrl}/getcuddleAvailable`,
                    config
                )
                .then((response) => {
                    if (response.status == 200) {

                        const result = response.data;

                        const cuddleList = result.cuddle_list;

                        // dynamic cuddle
                        const cuddleMaps = result.cuddle_maps;

                        // console.log(cuddleMaps);

                        this.setState(
                            (
                                {
                                    gameLabel: "cuddle",
                                    showcuddle: true,
                                    cuddleNames: cuddleList,
                                    cuddleMaps: cuddleMaps,
                                }
                            )
                        );

                    }
                })
                .catch((error) => {
                    console.log(error.response)
                    if (error.response) {
                        if (error.response.status == 401) {
                            Alert.alert(
                                `Session หมดอายุ กรุณาล็อกอินใหม่`,
                                `Session has expired, Please login`,
                            );
                        }
                        else if (error.response.status == 500) {
                            // console.log(error.response.status);
                            Alert.alert(
                                `Attention`,
                                `Oops! Try again next time`,
                            );
                        }
                        this.props.navigation.goBack();
                    }
                });
        }
        else if (this.state.gameType == "chooseCup") {
            axios.get
                (
                    `${apiUrl}/getCupsAvailable`,
                    config
                )
                .then((response) => {
                    if (response.status == 200) {

                        const result = response.data;

                        const gameList = result.game_list;
                        const cardList = result.card_list;

                        this.setState(
                            (
                                {
                                    gameLabel: "chooseCup",
                                    showchooseCup: true,
                                    showGameCard: false,

                                    chooseCupCards: cardList,
                                    chooseCups: gameList,
                                }
                            )
                        );

                    }
                })
                .catch((error) => {
                    console.log(error)
                    if (error.response) {
                        if (error.response.status == 401) {
                            Alert.alert(
                                `Session หมดอายุ กรุณาล็อกอินใหม่`,
                                `Session has expired, Please login`,
                            );
                        }
                        else if (error.response.status == 500) {
                            // console.log(error.response.status);
                            Alert.alert(
                                `Attention`,
                                `Oops! Try again next time`,
                            );
                        }
                        this.props.navigation.goBack();
                    }

                });
        }
    }

    getSubCup = (game) => {
        const gameNameAPI = this.getcupNameEntry(game);
        this.setState(({ selectedGame: gameNameAPI }));

        if (global.chaiyoGame.includes(game)) {
            this.play(0, game);
        } else {
            this.getcupRandom(game);
        }
    }

    getcupRandom = (game) => {
        const config = {
            headers: { Authorization: `Bearer ${this.state.token}` }
        };

        axios.post
            (
                `${apiUrl}/getcupRandom`,
                {
                    "game": game
                },
                config
            )
            .then((response) => {
                if (response.status == 200) {
                    const result = response.data.cups;

                    let ambcup = [];

                    if (result.length > 0) {
                        ambcup = result.filter((cup) => {
                            return cup.isActive == true;
                        })
                    }

                    // console.log(ambcup);

                    this.setState(
                        (
                            {
                                subcups: ambcup,
                                showSubcup: true,
                                showMaincup: false,
                            }
                        )
                    );

                }
            })
            .catch((error) => {
                if (error.response) {
                    if (error.response.status == 401) {
                        Alert.alert(
                            `Session หมดอายุ กรุณาล็อกอินใหม่`,
                            `Session has expired, Please login`,
                        );
                    }
                    else if (error.response.status == 500) {
                        Alert.alert(
                            `Attention`,
                            `Oops! Try again next time`,
                        );
                    }
                    this.state.navigation.goBack();
                }

            });
    }

    getcupNameEntry = (game) => {
        let gameName = "cupboard-" + game;
        return gameName;
    }

    play = (gameId, productCode, gametype) => {
        const payload = {};

        const cupName = this.state.selectedGame;

        console.log(gameId, productCode, gametype, cupName)

        let iscuddle = false;

        if (this.state.gameType == "cuddle") {
            iscuddle = true;
        }

        console.log(payload)

        const config = {
            headers: { Authorization: `Bearer ${this.state.token}` },
        };

        axios.post
            (
                `${apiUrl}/play`,
                payload,
                config
            )
            .then((response) => {
                if (response.status == 200) {
                    const url = response.data.url;

                    this.setState(
                        (
                            {
                                showcuddle: iscuddle ? true : false,
                                showSubcup: false,
                                showMaincup: true,
                            }
                        )
                    );

                    const supported = Linking.canOpenURL(url);

                    if (supported) {
                        Linking.openURL(url);
                    } else {
                        Alert.alert(`Can't open link : ${url}`);
                    }

                }
            })
            .catch((error) => {
                console.log(error.response.request._response);
                if (error.response) {
                    if (error.response.status == 401) {
                        Alert.alert(
                            `Session หมดอายุ กรุณาล็อกอินใหม่`,
                            `Session has expired, Please login`,
                        );
                        this.props.navigation.reset({ index: 0, routes: [{ name: 'Home' }], });
                    }
                    else if (error.response.status == 500) {
                        // console.log(error.response)
                        Alert.alert(
                            `Attention`,
                            `Oops! Try again next time`,
                        );
                        this.props.navigation.goBack();
                    }

                }

            });
    }

    switchchooseCup = (game) => {
        this.setState(
            (
                {
                    showchooseCup: game == "chooseCup" ? true : false,
                    showGameCard: game == "card" ? true : false,
                }
            )
        );
    }

    render() {

        const gameList = (
            <View style={gameStyles.gameListWrapper}>
                <ScrollView style={gameStyles.gameListScroll}>
                    {this.state.cupNames.map((cup, index) => {
                        return <CupButton key={index} data={cup} name={cup[0]} getSubCup={(name) => this.getSubCup(name)} />
                    })}
                </ScrollView>
            </View>
        );

        const subcupWidgets = (
            <View style={gameStyles.gameListWrapper}>
                <ScrollView style={gameStyles.gameListScroll}>
                    <View style={{ flex: 1, width: '100%', justifyContent: 'space-evenly', flexWrap: 'wrap', flexDirection: 'row' }}>
                        {this.state.subcups != undefined && this.state.subcups.map((cup, index) => {
                            return (
                                <TouchableOpacity key={index} onPress={() => this.play(cup.gameId, cup.productCode)} style={{ margin: 10, width: 100, height: 100, flexDirection: 'row' }}>
                                    <View>

                                        <View style={{ justifyContent: 'center', alignContent: 'center', alignSelf: 'center' }}>
                                            <WebView
                                                // Must set opacity and overflow for prevent crashing on Android!
                                                source={{
                                                    uri: cup.imgUrl
                                                }}
                                                style={{ width: 100, height: 150, opacity: 0.99, overflow: 'hidden' }}
                                            />
                                            <Text style={{ color: 'white', marginTop: 5, justifyContent: 'center', textAlign: 'center', fontFamily: 'sukhumvitset-thin-webfont', }}>{cup.gameName}</Text>

                                        </View>

                                    </View>
                                </TouchableOpacity>
                            );
                        })}
                    </View>

                </ScrollView>
            </View>
        );


        const cuddleWidgets = (
            <View style={gameStyles.widgetContainer}>
                <ScrollView>
                    <View style={{ width: '100%', justifyContent: 'space-around', flexWrap: 'wrap', flexDirection: 'row' }}>
                        {this.state.cuddleNames.map((cuddle, index) => {
                            // let path = global.imagePath[cuddle];
                            return (
                                <TouchableOpacity key={index} onPress={() => this.play(this.state.cuddleMaps[index][2], this.state.cuddleMaps[index][0], 'cuddle')} style={gameStyles.buttonWrapper}>
                                    <View>
                                        <View style={{ justifyContent: 'center', alignContent: 'center', alignSelf: 'center' }}>
                                            <Image resizeMode="contain" source={{ uri: this.state.cuddleMaps[index][1] }} style={gameStyles.imageCover} />
                                        </View>

                                    </View>
                                </TouchableOpacity>
                            );
                        })}
                    </View>
                </ScrollView>
            </View>
        );

        const chooseCupWidgets = (
            <View style={gameStyles.widgetContainer}>

                <ScrollView style={{}}>
                    <View style={{ width: '100%', justifyContent: 'space-around', flexWrap: 'wrap', flexDirection: 'row' }}>
                        {/* chooseCup */}
                        {this.state.showchooseCup && this.state.chooseCups.map((game, index) => {
                            let path = global.imagePath[game.cup_detail_name];
                            return (
                                <TouchableOpacity key={index} onPress={() => this.play(game.cup_detail_gameid, game.cup_name, 'chooseCup')} style={gameStyles.buttonWrapper}>
                                    <View>
                                        <View style={{ justifyContent: 'center', alignContent: 'center', alignSelf: 'center' }}>
                                            <Image resizeMode="contain" source={path} style={gameStyles.imageCover} />
                                        </View>
                                        <Text style={gameStyles.textUnderImage}>{game.cup_detail_name}</Text>
                                    </View>
                                </TouchableOpacity>
                            );
                        })}
                        {/* Card cups */}
                        {this.state.showGameCard && this.state.chooseCupCards.map((game, index) => {
                            return (
                                <TouchableOpacity key={index} onPress={() => this.play(game.gameId, 'ambgame', 'gamecard')} style={gameStyles.buttonWrapper}>
                                    <View>
                                        <View style={{ justifyContent: 'center', alignContent: 'center', alignSelf: 'center' }}>
                                            <Image resizeMode="contain" source={{ uri: game.thumbnail }} style={gameStyles.imageCover} />
                                        </View>
                                        <Text style={gameStyles.textUnderImage}>{game.name.th}</Text>
                                    </View>
                                </TouchableOpacity>
                            );
                        })}
                    </View>
                </ScrollView>
            </View>
        );


        const returnView = (

            <View style={gameStyles.container}>
                <HeaderMain
                    username={this.state.username}
                    navigation={this.props.navigation}
                    toTransactionPage={(page) => this.showTransactionPage(page)}
                />

                <View style={{ flex: 1 }}>
                    {/* Sub cup */}
                    {this.state.showMaincup && gameList}
                    {/* End sub cup */}

                    {/* Sub cup */}
                    {this.state.showSubcup && subcupWidgets}
                    {/* End sub cup */}

                    {/* cuddle */}
                    {this.state.showcuddle && cuddleWidgets}
                    {/* End cuddle */}

                    {/* chooseCup */}
                    {(this.state.showchooseCup || this.state.showGameCard) && chooseCupWidgets}
                    {/* End schooseCup */}
                </View>



                {/* Footer */}
                <Footer navigation={this.props.navigation} />
                {/* End Footer */}

            </View>




        );

        return returnView;
    }
}

const CupButton = (props) => {
    let data = props.data;
    return (
        <View style={gameStyles.cardWrapper}>
            <TouchableOpacity style={gameStyles.cupWrapper} onPress={() => props.getSubCup(props.name)}>
                <Image resizeMode="contain" source={{ uri: data[1] }} style={gameStyles.image} />
            </TouchableOpacity>
        </View>
    );

}

const gameStyles = StyleSheet.create({
    image: {
        width: '100%',
        height: '100%',
        alignSelf: 'center',
        justifyContent: 'center',
        alignContent: 'center',
    },
    container: {
        flex: 1,
        // margin: 12,
        // marginTop: '10%',
        // width: '100%',
    },
    header: {
        flex: .15,
        width: '100%',
        // borderWidth: 1,
    },
    gameListWrapper: {
        flex: 1,
        // borderWidth: 2,
    },
    gameListScroll: {
        // borderWidth: 1,
    },
    cardWrapper: {
        height: 100,
        margin: 10,
        marginTop: 35,
    },
    cupWrapper: {
        width: '70%',
        height: '100%',
        borderRadius: 5,
        alignSelf: 'center',
        justifyContent: 'center',
        alignContent: 'center',
        backgroundColor: 'transparent',
    },
    textTitle: {
        fontFamily: 'sukhumvitset-thin-webfont',
        alignSelf: 'center',
        textAlign: 'center',
        fontFamily: 'sukhumvitset-thin-webfont',
        fontSize: 29,
        color: 'white',
    },
    textButton: {
        fontFamily: 'sukhumvitset-thin-webfont',
        alignSelf: 'center',
        textAlign: 'center',
        fontSize: 20,
        color: 'white',
    },
    textButtonActive: {
        fontFamily: 'sukhumvitset-thin-webfont',
        alignSelf: 'center',
        textAlign: 'center',
        fontSize: 20,
        color: '#0fc2ff',
    },
    text: {
        fontFamily: 'sukhumvitset-thin-webfont',
        marginLeft: '10%',
        fontSize: 12,
        color: '#FF0D10',
    },
    errorText: {
        fontFamily: 'sukhumvitset-thin-webfont',
        marginLeft: '10%',
        fontSize: 12,
        color: '#FF0D10',
    },
    textUnderImage: {
        color: 'white',
        marginTop: 5,
        justifyContent: 'center',
        textAlign: 'center',
        fontFamily: 'sukhumvitset-thin-webfont',
    },
    widgetContainer: {
        marginTop: '10%',
        justifyContent: 'center',
        alignSelf: 'center',
        alignContent: 'center'
    },
    buttonWrapper: {
        alignSelf: 'center',
        borderWidth: 0,
        borderRadius: 15,
        margin: 5,
        width: 140,
        height: 150,
        flexDirection: 'row'
    },
    imageCover: {
        borderRadius: 5,
        width: 140,
        height: 150,
        justifyContent: 'center',
        alignSelf: 'center'
    },
    buttonTab: {
        width: '40%',
        backgroundColor: '#0d0d0d',
        margin: 10, borderWidth: 1, borderRadius: 5
    },
    buttonTabActive: {
        width: '40%',
        backgroundColor: '#1a1a1a',
        margin: 10, borderWidth: 1, borderRadius: 5
    },
});


const MainStyle = StyleSheet.create({
    mainContainer: {
        flex: 1,
    },
    loading: {
        position: 'absolute',
        alignSelf: 'center',
        alignItems: 'center',
        zIndex: 100,
        width: '100%',
        height: '100%',
        borderRadius: 10,
        backgroundColor: 'rgba(255,255,255,0.9)',
        justifyContent: 'center',

    },
    loadingText: {
        fontSize: 49
    },
    headerNav: {
        flex: .11,
        width: '100%',
        justifyContent: 'space-between',
        flexDirection: 'row',
        // borderWidth: 1,
        borderColor: 'white',
    },
    profileContainer: {
        marginLeft: 15,
        // borderWidth: 1,
        borderColor: 'white',
        justifyContent: 'center',
    },
    userProfile: {
        width: 35,
        height: 35,
        // borderWidth: 1,
        alignSelf: 'center',
        justifyContent: 'center',
    },
    usernameText: {
        fontSize: 9,
        color: 'white',
        alignSelf: 'center',
        alignContent: 'center',
        justifyContent: 'center',
    },
    mainPageLogoContainer: {
        alignContent: 'center',
        justifyContent: 'center',
        // borderWidth: 1,
        borderColor: 'white',
    },
    mainPageLogo: {
        width: 150,
        alignContent: 'center',
        justifyContent: 'center',
    },
    hamburgerMenu: {
        height: '100%',
        marginRight: '3%',
        alignSelf: 'flex-end',
        alignContent: 'center',
        justifyContent: 'center',
        // borderWidth: 1,
        borderColor: 'white',
    },
    hamburgerIcon: {
        width: 50,
        alignSelf: 'center',
        alignContent: 'center',
        justifyContent: 'center',
    },
    container: {
        flex: .9,
        marginTop: 45,
        alignItems: 'center',
        // borderWidth: 1,
        borderColor: 'green',
    },
    footerBackground: {
        position: 'absolute',
        bottom: 0,
        borderColor: '#426177',
        borderWidth: 1,
        width: '100%',
        height: 65,
        zIndex: -10,
    },
    footerMainButtonImage: {
        width: '100%',
        alignSelf: "center",
        justifyContent: 'center',
        height: '100%'
    },
    footerButtonImage: {
        width: 30,
        alignSelf: "center",
        justifyContent: 'center',
        // borderWidth: 1,
        height: 30
    },
    footer: {
        flex: .15,
        flexDirection: 'row',
        // flexWrap: 'wrap',
        justifyContent: 'space-between',
        alignContent: 'center',
        // borderWidth: 2,
        borderColor: 'white',
    },
    footerButton: {
        height: 70,
        width: '18%',
        borderRadius: 15,
        flexDirection: 'column',
        alignSelf: 'flex-end',
        alignContent: 'center',
        justifyContent: 'center',
        // borderWidth: 1,
        borderColor: 'white',
    },
    footerIcon: {
        fontSize: 10,
        color: 'white',
        justifyContent: 'center',
        alignSelf: 'center',
    },
    footerMain: {
        height: 90,
        width: '22%',
        borderRadius: 360,
        marginTop: 10,
        alignContent: 'center',
        justifyContent: 'center',
        borderColor: 'white',
        zIndex: 1,
    },

});



