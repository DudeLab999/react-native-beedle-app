import React, { Component } from 'react';
import {
    View, Text, StyleSheet, TextInput, ScrollView,
    Image, TouchableOpacity, Dimensions, Modal, TouchableWithoutFeedback,
} from "react-native";

import { LinearGradient } from 'expo-linear-gradient';
import { Ionicons } from '@expo/vector-icons';
import { Feather } from '@expo/vector-icons';
import Clipboard from 'expo-clipboard';

// Table
import axios from 'axios'
import { Table, TableWrapper, Row, Rows, Col } from 'react-native-table-component';

class Profile extends Component {
    constructor(props) {
        super(props)

        this.state = {
            firstname: this.props.firstname,
            lastname: this.props.lastname,
            nickname: this.props.nickname,
            username: this.props.username,
            phone: this.props.phone,
            bankNo: this.props.bankNo,
            bankName: this.props.bankName,
            bankCode: this.props.bankCode,
            test: this.props.test,
        }

    }

    render() {
        const returnView = (
            <View style={profileStlye.container}>
                <View style={profileStlye.title}>
                    <Image
                        style={profileStlye.image}
                        source={require("../assets/img/account_bt.png")}
                        resizeMode="contain"
                    />
                    <Text style={profileStlye.titleText}>Profile</Text>
                </View>

                <View style={{ marginTop: '5%' }}>
                    <Table borderStyle={{ borderWidth: 1, borderColor: '#C1C0B9' }}>
                        <Row data={['Name', <Text style={profileStlye.text}>{this.state.firstname}</Text>]} style={profileStlye.headerTable} textStyle={{ color: 'white', margin: 10 }} />
                        <Row data={['Surname', <Text style={profileStlye.text}>{this.state.lastname}</Text>]} style={profileStlye.headerTable} textStyle={{ color: 'white', margin: 10 }} />
                        <Row data={['Nickname', <Text style={profileStlye.text}>{this.state.nickname}</Text>]} style={profileStlye.headerTable} textStyle={{ color: 'white', margin: 10 }} />
                        <Row data={['Username', <Text style={profileStlye.text}>{this.state.username}</Text>]} style={profileStlye.headerTable} textStyle={{ color: 'white', margin: 10 }} />
                        <Row data={['Wallet', <Text style={profileStlye.text}>{this.state.bankNo}</Text>]} style={profileStlye.headerTable} textStyle={{ color: 'white', margin: 10 }} />
                        <Row data={['Bank', <Text style={profileStlye.text}>{this.state.bankName}</Text>]} style={profileStlye.headerTable} textStyle={{ color: 'white', margin: 10 }} />
                        <Row data={['Phone', <Text style={profileStlye.text}>{this.state.phone}</Text>]} style={profileStlye.headerTable} textStyle={{ color: 'white', margin: 10 }} />
                    </Table>
                </View>

                <View style={profileStlye.buttonWrapper}>
                    <LinearGradient style={profileStlye.button}
                        colors={['#2980B9', '#56afbf']}
                        start={[1, 0]}
                        end={[1, 1]}
                    >
                        <TouchableOpacity onPress={() => this.props.changePassword()}>
                            <Text style={profileStlye.buttonText}>Change Password</Text>
                        </TouchableOpacity>
                    </LinearGradient>

                    <LinearGradient style={profileStlye.button}
                        colors={['#223041', '#212529', '#426177']}
                        start={[1, 0]}
                        end={[1, 1]}
                    >
                        <TouchableOpacity onPress={() => this.props.logout()}>
                            <Text style={profileStlye.buttonText}>Logout</Text>
                        </TouchableOpacity>
                    </LinearGradient>

                </View>

            </View>
        );
        return returnView;
    }

}

export default Profile;

const profileStlye = StyleSheet.create({
    container: {
        // borderWidth: 1,
        flex: 1,
        marginTop: '10%',
        width: '90%',
    },
    title: {
        // borderWidth: 1,
        flex: .2,
        // width: '100%',
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignSelf: 'center',
    },
    image: {
        // borderWidth: 1,
        height: '100%',
        width: '10%',
        alignSelf: 'center',
        justifyContent: 'center',
        alignContent: 'center',
    },
    titleText: {
        fontSize: 24,
        textAlign: 'center',
        fontFamily: 'sukhumvitset-thin-webfont',
        color: 'white',
        fontWeight: '600',
        marginLeft: 5,
        alignSelf: 'center',
        justifyContent: 'center',
        alignContent: 'center',
    },
    text: {
        fontSize: 14,
        color: 'white',
        fontWeight: '500',
        textAlign: 'right',
        fontFamily: 'sukhumvitset-thin-webfont',
        margin: 10,
    },
    headerTable: {
        justifyContent: 'center',
        alignContent: 'center',
        height: 40,
        backgroundColor: '#181818',
    },
    buttonWrapper: {
        flex: .3,
        marginTop: '10%',
        justifyContent: 'center',
        alignContent: 'center',
    },
    button: {
        width: '60%',
        height: '50%',
        marginTop: '5%',
        borderRadius: 5,
        alignSelf: 'center',
        justifyContent: 'center',
        alignContent: 'center',
    },
    buttonText: {
        fontSize: 18,
        color: 'white',
        fontWeight: '600',
        textAlign: 'center',
        fontFamily: 'sukhumvitset-thin-webfont',
        justifyContent: 'center',
        alignContent: 'center',
    },

});