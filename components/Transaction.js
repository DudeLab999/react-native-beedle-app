import React, { Component } from 'react';
import {
    View, Text, StyleSheet, TextInput, ScrollView, ActivityIndicator,
    Image, TouchableOpacity, Alert
} from "react-native";

import HeaderMain from '../components/HeaderMain';
import Footer from '../components/Footer';

import { Ionicons } from '@expo/vector-icons';
import { Feather } from '@expo/vector-icons';
import Clipboard from 'expo-clipboard';

// Table
import axios from 'axios'
import { Table, Row } from 'react-native-table-component';

require('../global.js');

const apiUrl = global.API_URL;

const DEPOSIT = 0;
const WITHDRAW = 1;
const HISTORY = 2;
const BANK_TRANSFER = 0;
const QR_PAYMENT = 1;

class Transaction extends Component {
    constructor(props) {
        super(props)

        let oldState = props.route.params;
        const bankIconPath = this.getBankIcon(oldState.userBankCode);
        const cupWalletIconPath = this.getBankIcon(oldState.cupWalletCode);

        this.state = {
            token: oldState.token,
            refreshToken: oldState.refreshToken,
            expireIn: oldState.expires_in,
            loading: false,
            modalVisible: false,

            firstname: oldState.firstname,
            lastname: oldState.lastname,
            nickname: oldState.nickname,
            username: oldState.username,
            surname: oldState.surname,
            phone: oldState.phone,
            userBankLogo: bankIconPath,
            userBankCode: oldState.userBankCode,
            userBankName: oldState.userBankName,
            userBankNo: oldState.userBankNo,
            cupWalletCode: oldState.cupWalletCode,
            cupWalletLogo: cupWalletIconPath,
            cupWalletName: oldState.cupWalletName,
            cupWalletNo: oldState.cupWalletNo,
            cupWalletOwner: oldState.cupWalletOwner,
            balance: oldState.balance,
            comBalance: Number(oldState.commission),

            userBankAccount: oldState.userBankNo,
            selectedPayment: BANK_TRANSFER,
            selectedPaymentPage: oldState.transactionPageActive,
            transactionPageActive: oldState.transactionPageActive,

            defaultBankTransfer: {
                bankName: oldState.cupWalletName,
                bankNo: oldState.cupWalletNo,
                owner: oldState.cupWalletOwner,
                imgPath: cupWalletIconPath,
            },
            clipboardStatus: 0,
            qrReady: 2,
            qrAmountCreate: 0,

            tableHead: ['วันที่', 'ผู้แจ้ง', 'ประเภท', 'ธนาคาร', 'ชื่อบัญชี', 'เลขที่', 'จำนวน', 'สถานะ'],
            widthArr: [200, 160, 80, 100, 120, 140, 160, 180, 40],
            tableData: [],

            withdrawAmount: 0,
        }

    }

    getBankIcon = (bankCode) => {

        let img = require('../assets/img/bank_icons/kbank.png');

        switch (bankCode) {
            case 'kbank':
                img = require('../assets/img/bank_icons/kbank.png');
                break;

            case 'scb':
                img = require('../assets/img/bank_icons/scb.jpeg');
                break;

            case 'ktb':
                img = require('../assets/img/bank_icons/ktb.png');
                break;

            case 'bbl':
                img = require('../assets/img/bank_icons/bbl.jpg');
                break;

            case 'bay':
                img = require('../assets/img/bank_icons/bay.png');
                break;

            case 'tbank':
                img = require('../assets/img/bank_icons/tbank.png');
                break;

            case 'tmb':
                img = require('../assets/img/bank_icons/tmb.png');
                break;

            case 'gsb':
                img = require('../assets/img/bank_icons/gsb.jpg');
                break;
            case 'bag':
                img = require('../assets/img/bank_icons/bag.png');
                break;

            default:
                img = require('../assets/img/bank_icons/kbank.png');
        }

        return img;
    }

    setModalVisible = (status) => {
        this.setState(
            (
                {
                    modalVisible: status
                }
            )
        );
    }

    showMainPage = () => {
        this.props.navigation.navigate("Main", { ...this.state });
    }

    componentDidMount() {
        // TODO fetch DATA
    }

    switchTransactionPage = (flag, from) => {
        if (flag === undefined || flag === null) {
            flag = 0;
        }
        this.setState(
            (
                {
                    transactionPageActive: flag,
                    modalVisible: false,
                }
            )
        );
        if (flag == HISTORY) {
            this.setState(({
                tableData: this.fetchTransactionHistory()
            }))
        }
    }

    selectPayment = (payment) => {
        if (payment === undefined || payment === null) {
            payment = 0;
        }
        this.setState(
            (
                {
                    selectedPayment: payment,
                    qrReady: 2,
                }
            )
        );
    }

    copyBankAcc = async () => {
        Clipboard.setString(this.state.defaultBankTransfer.bankNo);
        this.setState(
            (
                {
                    clipboardStatus: 1
                }
            )
        );
        setTimeout(() => {
            this.setState(
                (
                    {
                        clipboardStatus: 0
                    }
                )
            );
        }, 1300);
    };

    updateWithdrawAmount = (amount) => {
        this.setState({ withdrawAmount: amount })
    }

    withdraw = () => {
        const reg = /^[0-9\b]+$/;
        if (this.state.withdrawAmount <= 0 || !reg.test(this.state.withdrawAmount)) {
            Alert.alert("Invalid score withdraw", "Invalid score withdraw");
        } else if (this.state.withdrawAmount > this.state.balance) {
            Alert.alert("Insufficient score!");
        }
        else {
            // Set loading
            this.setState({ loading: true });

            // withdrawAPI
            // Use nickname
            const config = {
                headers: { Authorization: `Bearer ${this.state.token}` }
            };
            axios.post
                (
                    `${apiUrl}/withdrawScore`,
                    {
                        'username': this.state.nickname,
                        'score': Number(this.state.withdrawAmount)
                    },
                    config
                )
                .then((response) => {
                    if (response.status == 200) {

                        const result = response.data;

                        if (result.status) {
                            Alert.alert(
                                result.status
                            );
                        } else {
                        }

                        this.setState({ loading: false, withdrawAmount: 0 })

                    }
                })
                .catch((error) => {
                    this.setState({ loading: false, withdrawAmount: 0 })

                    if (error.response) {
                        if (error.response.status == 401) {
                            Alert.alert(
                                `Session หมดอายุ กรุณาล็อกอินใหม่`,
                                `Session has expired, Please login`,
                            );
                        }
                        this.props.navigation.reset({ index: 0, routes: [{ name: 'Home' }], });
                    }
                });
        }

    }

    fetchTransactionHistory = () => {
        let tableData = [];
        const config = {
            headers: { Authorization: `Bearer ${this.state.token}` }
        };

        axios.post
            (
                `${apiUrl}/getTransactionHistory`,
                {
                    "username": this.state.username
                },
                config
            )
            .then((response) => {
                if (response.status == 200) {
                    console.log(response.data);
                    if (response.data.result) {
                        tableData = response.data.result;

                        this.setState(
                            (
                                {
                                    tableData: tableData,
                                    records: response.data.result
                                }
                            )
                        );
                    }

                }
            })
            .catch((error) => {
                // console.log(error);
                if (error.response) {
                    if (error.response.status == 401) {
                        // console.log(error.response.status);
                        Alert.alert(
                            `Session หมดอายุ กรุณาล็อกอินใหม่`,
                            `Session expired, Please login`,
                        );
                    }
                    this.props.navigation.reset({ index: 0, routes: [{ name: 'Home' }], });
                }
            });
        return tableData;
    }

    render() {
        const state = this.state;
        const tableData = this.state.tableData;

        const transactionWidgets = (

            <View style={MainStyle.mainContainer}>
                <HeaderMain
                    username={this.state.username}
                    navigation={this.props.navigation}
                    toTransactionPage={(page) => this.switchTransactionPage(page, 1)}
                />
                {this.state.loading && <View style={MainStyle.loading}><ActivityIndicator size="large" color="black" /></View>}
                <View style={MainStyle.container}>
                    <View style={transactionStyle.headerNav}>
                    </View>

                </View>

                {/* Footer */}
                <Footer navigation={this.props.navigation} />
                {/* End Footer */}

            </View>

        );



        return transactionWidgets;
    }

}

export default Transaction;


const styles = StyleSheet.create({
    container: { flex: 1, padding: 16, paddingTop: 30, backgroundColor: 'transparent' },
    header: { height: 50, backgroundColor: '#181818' },
    text: { color: 'white', textAlign: 'center', fontFamily: 'sukhumvitset-thin-webfont', },
    dataWrapper: { marginTop: -1 },
    row: { height: 40, backgroundColor: '#222222' }
});

const transactionStyle = StyleSheet.create({
    headerNav: {
        flex: .1,
        // marginTop: 10,
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
        alignContent: 'center',
        // borderWidth: 1,
        borderColor: 'white',
    },
    transactionButton: {
        flex: 1,
        width: '25%',
        height: '70%',
        margin: 5,
        alignSelf: 'center',
        justifyContent: 'center',
        alignContent: 'center',
        backgroundColor: '#0d0d0d',
        // borderWidth: 1,
        borderRadius: 5,
    },
    inputWithdraw: {
        // marginTop: 10,
        justifyContent: 'center',
        textAlign: 'center',
        fontFamily: 'sukhumvitset-thin-webfont',
        alignContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        width: '80%',
        height: '30%',
        color: 'white',
        borderWidth: 1,
        borderColor: '#426177',
    },
    withdrawButton: {
        width: '50%',
        height: '40%',
        marginTop: 10,
        borderRadius: 10,
        alignSelf: 'center',
        justifyContent: 'center',
        alignContent: 'center',
        backgroundColor: '#569fbd',
    },
    transactionButtonActive: {
        flex: 1,
        width: '25%',
        height: '70%',
        margin: 5,
        alignSelf: 'center',
        justifyContent: 'center',
        alignContent: 'center',
        borderColor: '#d6c981',
        backgroundColor: '#1a1a1a',//'#1a1a1a',
        borderWidth: 1,
        borderRadius: 5,
    },
    textOnButton: {
        color: 'white',
        alignSelf: 'center',
        fontSize: 16,
    },
    textOnButton2: {
        color: 'white',
        alignSelf: 'center',
        fontSize: 16,
    },
    noteText: {
        color: 'white',
        margin: 10,
        alignSelf: 'center',
        justifyContent: 'center',
        alignContent: 'center',
    },
    bankDepositContainer: {
        flex: 1,
        width: '90%',
        // borderWidth: 1,
        // backgroundColor: 'white',
    },
    paymentMethodWrapper: {
        flex: 0.1,
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
        alignContent: 'center',
        // borderWidth: 2,
        borderColor: 'green',
    },
    paymentMethodButton: {
        width: '40%',
        height: '80%',
        margin: 5,
        justifyContent: 'center',
        alignContent: 'center',
        borderWidth: 1,
        borderRadius: 5,
        backgroundColor: '#1a1a1a',
    },
    paymentMethodButtonActive: {
        width: '40%',
        height: '80%',
        margin: 5,
        justifyContent: 'center',
        alignContent: 'center',
        borderWidth: 1,
        borderRadius: 5,
        borderColor: '#d6c981',
        backgroundColor: '#1a1a1a',
    },
    qrWrapper: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
        alignContent: 'center',
        // borderWidth: 1,
    },
    qrIcon: {
        marginRight: 5,
        alignSelf: 'center',
        justifyContent: 'center',
        alignContent: 'center',
    },
    bankInfoContainer: {
        flex: 0.8,
        marginTop: 5,
        borderWidth: 1,
        borderColor: 'white',
    },
    bankInfoLogoWrapper: {
        flex: .3,
        marginTop: '15%',
        justifyContent: 'center',
        alignContent: 'center',
        // borderWidth: 1,
    },
    bankInfoWrapper: {
        flex: .7,
        marginTop: 20,
        alignContent: 'center',
        // borderWidth: 1,
    },
    bankLogo: {
        width: '50%',
        height: '100%',
        justifyContent: 'center',
        alignContent: 'center',
        alignSelf: 'center'
    },
    bankNameText: {
        color: 'white',
        fontSize: 16,
        justifyContent: 'center',
        alignContent: 'center',
        alignSelf: 'center'
    },
    bankOwnerText: {
        color: 'white',
        fontSize: 18,
        justifyContent: 'center',
        alignContent: 'center',
        alignSelf: 'center'
    },
    bankNoText: {
        color: '#569fbd',
        fontSize: 16,
        justifyContent: 'center',
        alignContent: 'center',
        alignSelf: 'center'
    },
    copyButton: {
        marginTop: '5%',
        width: '25%',
        height: '20%',
        flexDirection: 'row',
        justifyContent: 'center',
        alignContent: 'center',
        alignSelf: 'center',
        borderWidth: 1,
        borderRadius: 3,
        backgroundColor: 'rgba(255,255,255,0.8)',
    },
    copyText: {
        color: 'black',
        alignSelf: 'center',
        fontSize: 12,
    },
    copyIcon: {
        marginLeft: 5,
        alignSelf: 'center',
        justifyContent: 'center',
        alignContent: 'center',
    },
    copySuccessWrapper: {
        width: '50%',
        height: '25%',
        marginTop: '15%',
        justifyContent: 'center',
        alignSelf: 'center',
        borderWidth: 1,
        borderRadius: 32,
        backgroundColor: "rgba(255,255,255,0.9)",
    },
    copiedText: {
        color: 'black',
        alignSelf: 'center',
        fontSize: 12,
    },
    qrPaymentWrapper: {
        // flex: 1,
        // borderWidth: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignContent: 'center',
    },
    qrImage: {
        width: '50%',
        height: '80%',
        justifyContent: 'center',
        alignContent: 'center',
        alignSelf: 'center'
    },
    qrCaption: {
        color: 'white',
        justifyContent: 'center',
        alignContent: 'center',
        alignSelf: 'center',
    },
    qrAlertWrapper: {
        flex: 1,
        borderWidth: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignContent: 'center',
    },
    qrAlertText1: {
        color: 'orange',
        justifyContent: 'center',
        alignContent: 'center',
        alignSelf: 'center',
    },
    input: {
        marginTop: 20,
        justifyContent: 'center',
        textAlign: 'center',
        fontFamily: 'sukhumvitset-thin-webfont',
        alignContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        width: '80%',
        height: '30%',
        color: 'white',
        borderWidth: 1,
        borderColor: '#426177',
    },
    withdrawFromWrapper: {
        flex: .4,
        width: '100%',
        flexDirection: 'row',
        flexWrap: 'wrap',
        // borderWidth: 1,
    },
    userInfoWrapper: {
        width: '100%',
        height: '80%',
        // marginLeft: 10,
        justifyContent: 'center',
        alignSelf: 'center',
        flexDirection: 'column',
        // borderWidth: 1,
    },
    userLogo: {
        width: '60%',
        height: '50%',
        justifyContent: 'center',
        alignSelf: 'center',
    },
    withdrawToWrapper: {
        flex: .4,
        width: '100%',
        flexWrap: 'wrap',
        flexDirection: 'row',
    },
    withdrawAmountWrapper: {
        flex: .3,
        width: '100%',
        flexWrap: 'wrap',
        flexDirection: 'row',
    },
    withdrawTitle: {
        fontSize: 20,
        color: 'white',
        fontWeight: '300',
        marginTop: 10,
        marginLeft: 20,
        justifyContent: 'flex-start',
        alignSelf: 'flex-start',
        zIndex: 10
        // borderWidth: 1
    },
    withdrawSubText: {
        fontSize: 12,
        color: 'white',
        fontWeight: '300',
        marginTop: 10,
        alignSelf: 'center',
        justifyContent: 'center',
    },
    withdrawAmountText: {
        fontSize: 16,
        color: '#569fbd',
        fontWeight: '300',
        marginTop: 10,
        alignSelf: 'center',
        justifyContent: 'center',
    }


});

const MainStyle = StyleSheet.create({
    mainContainer: {
        flex: 1,
    },
    loading: {
        position: 'absolute',
        alignSelf: 'center',
        alignItems: 'center',
        zIndex: 100,
        width: '100%',
        height: '100%',
        borderRadius: 10,
        backgroundColor: 'rgba(255,255,255,0.9)',
        justifyContent: 'center',

    },
    loadingText: {
        fontSize: 49
    },
    headerNav: {
        flex: .11,
        width: '100%',
        justifyContent: 'space-between',
        flexDirection: 'row',
        // borderWidth: 1,
        borderColor: 'white',
    },
    profileContainer: {
        marginLeft: 15,
        // borderWidth: 1,
        borderColor: 'white',
        justifyContent: 'center',
    },
    userProfile: {
        width: 35,
        height: 35,
        // borderWidth: 1,
        alignSelf: 'center',
        justifyContent: 'center',
    },
    usernameText: {
        fontSize: 9,
        color: 'white',
        alignSelf: 'center',
        alignContent: 'center',
        justifyContent: 'center',
    },
    mainPageLogoContainer: {
        alignContent: 'center',
        justifyContent: 'center',
        // borderWidth: 1,
        borderColor: 'white',
    },
    mainPageLogo: {
        width: 150,
        alignContent: 'center',
        justifyContent: 'center',
    },
    hamburgerMenu: {
        height: '100%',
        marginRight: '3%',
        alignSelf: 'flex-end',
        alignContent: 'center',
        justifyContent: 'center',
        // borderWidth: 1,
        borderColor: 'white',
    },
    hamburgerIcon: {
        width: 50,
        alignSelf: 'center',
        alignContent: 'center',
        justifyContent: 'center',
    },
    container: {
        flex: .9,
        // marginTop: 45,
        alignItems: 'center',
        // borderWidth: 1,
        borderColor: 'green',
    },
    footerBackground: {
        position: 'absolute',
        bottom: 0,
        borderColor: '#426177',
        borderWidth: 1,
        width: '100%',
        height: 65,
        zIndex: -10,
    },
    footerMainButtonImage: {
        width: '100%',
        alignSelf: "center",
        justifyContent: 'center',
        height: '100%'
    },
    footerButtonImage: {
        width: 30,
        alignSelf: "center",
        justifyContent: 'center',
        // borderWidth: 1,
        height: 30
    },
    footer: {
        flex: .15,
        flexDirection: 'row',
        // flexWrap: 'wrap',
        justifyContent: 'space-between',
        alignContent: 'center',
        // borderWidth: 2,
        borderColor: 'white',
    },
    footerButton: {
        height: 70,
        width: '18%',
        borderRadius: 15,
        flexDirection: 'column',
        alignSelf: 'flex-end',
        alignContent: 'center',
        justifyContent: 'center',
        // borderWidth: 1,
        borderColor: 'white',
    },
    footerIcon: {
        fontSize: 10,
        color: 'white',
        justifyContent: 'center',
        alignSelf: 'center',
    },
    footerMain: {
        height: 90,
        width: '22%',
        borderRadius: 360,
        marginTop: 10,
        alignContent: 'center',
        justifyContent: 'center',
        borderColor: 'white',
        zIndex: 1,
    },
});