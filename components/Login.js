import React, { Component } from 'react';
import { Dimensions, View, Text, Button, StyleSheet, TextInput } from "react-native";
import { LinearGradient } from 'expo-linear-gradient';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Formik } from 'formik';
import * as Yup from 'yup';

require('../global.js');

const LoginSchema = Yup.object().shape({

    username: Yup.string()
        .min(6, 'Invalid input')
        .max(30, 'You shall not pass!')
        .required('Fill me'),

    password: Yup.string()
        .min(4, 'Invalid input')
        .max(50, 'You shall not pass!')
        .required('Fill me'),
});

const loginAPIUrl = global.API_URL + "/login";

export default class Login extends Component {
    constructor(props) {
        super(props);
        const dimensions = Dimensions.get('window');

        this.state = {
            token: null,
            refreshToken: null,
            expireIn: 3600,
        }
    }

    login = async (values) => {
        let response = await fetch(
            loginAPIUrl, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "username": values.username,
                "password": values.password
            })
        }).catch((error) => {
            console.log(error);
        });

        let json = await response.json();

        if (json.status === 200) {
            this.setState(({
                token: json.access_token,
                expireIn: json.expires_in,
            }))

            this.props.LoginSuccess(json);
        } else {
            alert("Username Or Password Incorrect");
        }

    }

    liveChat = () => {
        // console.log("sss")
        this.props.liveChat();
    }

    register = () => {
        this.props.Register();
    }

    render() {
        return (
            <View style={loginStyles.loginContainer}>
                {/* Login Form */}
                <Formik
                    initialValues={{
                        username: '',
                        password: '',
                    }}
                    onSubmit={values => {
                        this.login(values);
                    }}
                    validationSchema={LoginSchema}
                >
                    {({ values, handleChange, errors, setFieldTouched, touched, isValid, handleSubmit }) => (
                        <View style={loginStyles.container}>

                            <TextInput
                                style={loginStyles.input}
                                placeholder="USERNAME"
                                placeholderTextColor="grey"
                                autoFocus={true}
                                textContentType="username"

                                value={values.username}
                                onChangeText={handleChange('username')}
                                onBlur={() => setFieldTouched('username')}

                            />
                            {touched.username && errors.username &&
                                <Text style={loginStyles.errorText}>{errors.username}</Text>
                            }

                            <TextInput
                                style={loginStyles.input}
                                placeholder="PASSWORD"
                                placeholderTextColor="grey"
                                textContentType="password"
                                secureTextEntry={true}

                                value={values.password}
                                onChangeText={handleChange('password')}
                                onBlur={() => setFieldTouched('password')}
                            />

                            {touched.password && errors.password &&
                                <Text style={loginStyles.errorText}>{errors.password}</Text>
                            }

                            {/* Forgot Password */}
                            <TouchableOpacity style={loginStyles.forgotPasswordWrapper} onPress={() => this.liveChat()}>
                                <Text style={loginStyles.forgotPassword}>Forgot</Text>
                            </TouchableOpacity>

                            <View style={loginStyles.spaceBetweenButton}></View>

                            {/* Buttons */}
                            <LinearGradient style={loginStyles.linearGradient}
                                colors={['#223041', '#19242d']}
                                start={[1, 0.6]}
                                end={[1, 1]}
                            >
                                <TouchableOpacity style={loginStyles.buttonRegister} onPress={handleSubmit}>
                                    <Text style={loginStyles.textButton}>Login</Text>
                                </TouchableOpacity>
                            </LinearGradient>

                            <LinearGradient style={loginStyles.linearGradient}
                                colors={['rgba(1,1,1,0.1)', 'rgba(1,1,1,0.1)', 'rgba(1,1,1,0.1)', 'transparent']}>
                                <TouchableOpacity style={loginStyles.buttonRegister} onPress={() => this.register()}>
                                    <Text style={loginStyles.textButton}>Register</Text>
                                </TouchableOpacity>
                            </LinearGradient>

                        </View>
                    )}
                </Formik>

            </View>

        );
    }
}

const loginStyles = StyleSheet.create({
    loginContainer: {
        justifyContent: 'center',
        margin: 12,
        marginTop: '45%',
        // borderWidth: 3,
        borderColor: 'white',
        marginBottom: '20%',
    },
    linearGradient: {
        width: '85%',
        alignSelf: 'center',
        justifyContent: 'center',
        alignContent: 'center',
        borderWidth: 0.8,
        borderColor: '#223041',
        borderRadius: 32,
        marginBottom: 10,
    },
    buttonRegister: {
        height: 60,
        textAlign: 'center',
        marginHorizontal: 20,
        justifyContent: 'center',
        zIndex: 1,
        // fontFamily: 'sukhumvitset-thin-webfont',
    },
    forgotPasswordWrapper: {
        // borderWidth: 1,
        alignSelf: 'flex-end',
        marginRight: '5%',
        // flexWrap: 'wrap',
        // width: '40%',
        // height: 60,
        zIndex: 1,
    },
    textButton: {
        fontFamily: 'sukhumvitset-thin-webfont',
        fontSize: 15,
        color: 'white',
        textAlign: 'center',
        // fontFamily: 'sukhumvitset-thin-webfont',
    },
    input: {
        height: 40,
        margin: 12,
        // borderWidth: 1,
        // borderRadius: 10,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        borderBottomWidth: 0.5,
        color: 'white',
        borderColor: 'white',
        justifyContent: 'center',
        paddingHorizontal: 10,
    },
    spaceBetweenButton: {
        marginBottom: '10%',
    },
    forgotPassword: {
        color: 'white',
        opacity: 0.5,
        textDecorationLine: 'underline',
        alignSelf: 'flex-end'
    },
    errorText: {
        marginLeft: '10%',
        fontSize: 12,
        color: '#FF0D10',
    },
});



