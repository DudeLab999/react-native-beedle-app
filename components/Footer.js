import React, { Component } from 'react';
import {
    View, Text, StyleSheet,
    Image, TouchableOpacity
} from "react-native";
import { LinearGradient } from 'expo-linear-gradient';

export default class Footer extends Component {
    constructor(props) {
        super(props);
    }


    render() {
        return (
            <>
                {/* Footer */}
                <View style={MainStyle.footer}>
                    <TouchableOpacity style={MainStyle.footerButton}
                        onPress={() => this.props.navigation.navigate("Main")}
                    >
                        <Image
                            resizeMode='contain'
                            source={require("../assets/img/home_bottom_icon.png")}
                            style={MainStyle.footerButtonImage}
                        />
                        <Text style={MainStyle.footerIcon}>Home</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate("Promotion")}
                        style={MainStyle.footerButton}>
                        <Image
                            resizeMode='contain'
                            source={require("../assets/img/promotion_bottom_icon.png")}
                            style={MainStyle.footerButtonImage}
                        />
                        <Text style={MainStyle.footerIcon}>Promotion</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={MainStyle.footerMain}
                        onPress={() => this.props.navigation.navigate("Main")}
                    >
                        <Image
                            resizeMode='contain'
                            source={require("../assets/img/cup_bt.png")}
                            style={MainStyle.footerMainButtonImage}
                        />
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => this.props.navigation.navigate("Announce", { ...this.state })}
                        style={MainStyle.footerButton}>
                        <Image
                            resizeMode='contain'
                            source={require("../assets/img/cup_bt.png")}
                            style={MainStyle.footerButtonImage}
                        />
                        <Text style={MainStyle.footerIcon}>Announce</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={MainStyle.footerButton}
                        onPress={() => this.props.navigation.navigate("Announce", { ...this.state })}
                    // onPress={() => this.props.navigation().navigate("Chat")}
                    >
                        <Image
                            resizeMode='contain'
                            source={require("../assets/img/contact_bottom_bt.png")}
                            style={MainStyle.footerButtonImage}
                        />
                        <Text style={MainStyle.footerIcon}>Contact</Text>
                    </TouchableOpacity>
                </View>


                <LinearGradient style={MainStyle.footerBackground}
                    colors={['#223041', '#212529', '#426177']}
                    start={[1, 0]}
                    end={[1, 1]}
                >
                </LinearGradient>

            </>
        )
    }

}

const MainStyle = StyleSheet.create({
    mainContainer: {
        flex: 1,
    },
    loading: {
        position: 'absolute',
        alignSelf: 'center',
        alignItems: 'center',
        zIndex: 100,
        width: '100%',
        height: '100%',
        borderRadius: 10,
        backgroundColor: 'rgba(255,255,255,0.9)',
        justifyContent: 'center',

    },
    loadingText: {
        fontSize: 49
    },
    headerNav: {
        flex: .11,
        width: '100%',
        justifyContent: 'space-between',
        flexDirection: 'row',
        // borderWidth: 1,
        borderColor: 'white',
    },
    profileContainer: {
        marginLeft: 15,
        // borderWidth: 1,
        borderColor: 'white',
        justifyContent: 'center',
    },
    userProfile: {
        width: 35,
        height: 35,
        // borderWidth: 1,
        alignSelf: 'center',
        justifyContent: 'center',
    },
    usernameText: {
        fontSize: 9,
        color: 'white',
        alignSelf: 'center',
        alignContent: 'center',
        justifyContent: 'center',
    },
    mainPageLogoContainer: {
        alignContent: 'center',
        justifyContent: 'center',
        // borderWidth: 1,
        borderColor: 'white',
    },
    mainPageLogo: {
        width: 150,
        alignContent: 'center',
        justifyContent: 'center',
    },
    hamburgerMenu: {
        height: '100%',
        marginRight: '3%',
        alignSelf: 'flex-end',
        alignContent: 'center',
        justifyContent: 'center',
        // borderWidth: 1,
        borderColor: 'white',
    },
    hamburgerIcon: {
        width: 50,
        alignSelf: 'center',
        alignContent: 'center',
        justifyContent: 'center',
    },
    container: {
        flex: .9,
        marginTop: 45,
        alignItems: 'center',
        // borderWidth: 1,
        borderColor: 'green',
    },
    footerBackground: {
        position: 'absolute',
        bottom: 0,
        borderColor: '#426177',
        borderWidth: 1,
        width: '100%',
        height: 65,
        zIndex: -10,
    },
    footerMainButtonImage: {
        width: '100%',
        alignSelf: "center",
        justifyContent: 'center',
        height: '100%'
    },
    footerButtonImage: {
        width: 30,
        alignSelf: "center",
        justifyContent: 'center',
        // borderWidth: 1,
        height: 30
    },
    footer: {
        flex: .2,
        flexDirection: 'row',
        // flexWrap: 'wrap',
        justifyContent: 'space-between',
        alignContent: 'center',
        // borderWidth: 2,
        borderColor: 'white',
    },
    footerButton: {
        height: 70,
        width: '18%',
        borderRadius: 15,
        flexDirection: 'column',
        alignSelf: 'flex-end',
        alignContent: 'center',
        justifyContent: 'center',
        // borderWidth: 1,
        borderColor: 'white',
    },
    footerIcon: {
        fontSize: 10,
        color: 'white',
        justifyContent: 'center',
        alignSelf: 'center',
    },
    footerMain: {
        height: 90,
        width: '22%',
        borderRadius: 360,
        marginTop: 10,
        alignContent: 'center',
        justifyContent: 'center',
        borderColor: 'white',
        zIndex: 1,
    },
});


