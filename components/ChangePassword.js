import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput, TouchableOpacity } from "react-native";
import { LinearGradient } from 'expo-linear-gradient';

import HeaderMain from '../components/HeaderMain';
import axios from 'axios';

require('../global.js');

const apiUrl = global.API_URL;

const OLD = 0;
const NEW = 1;
const CONFIRM = 2;
const invalidText = "กรุณากรอกรหัสผ่าน";
const notmatchText = "รหัสผ่านไม่ตรงกัน";

export default class ChangePassword extends Component {
    constructor(props) {
        super(props);

        // console.log(this.props.route.params.token);

        this.state = {
            keyGen: 1,
            token: this.props.route.params.token,
            username: this.props.route.params.username,
            oldPassword: "",
            newPassword: "",
            checkPassword: "",

            errorOldPassword: "",
            errorNewPassword: "",
            errorCheckPassword: "",

            formMessage: "",
            disableButton: false,
        }
    }

    checkPassword = (text, flag) => {
        if (flag == OLD) {
            if (text != "") {
                this.setState(({
                    errorOldPassword: "",
                    oldPassword: text,
                }));
            } else {
                this.setState(({
                    errorOldPassword: invalidText,
                    oldPassword: text,
                }));
            }
        }
        else if (flag == NEW) {
            if (text != "") {
                this.setState(({
                    errorNewPassword: "",
                    newPassword: text,
                }));
            } else {
                this.setState(({
                    errorNewPassword: invalidText,
                    newPassword: text,
                }));
            }

            if (this.state.checkPassword != text) {
                this.setState(({
                    errorCheckPassword: notmatchText,
                    newPassword: text,
                }));
            }
        }
        else if (flag == CONFIRM) {
            if (text != "") {
                this.setState(({
                    errorCheckPassword: "",
                    checkPassword: text,
                }));
            } else {
                this.setState(({
                    errorCheckPassword: invalidText,
                    checkPassword: text,
                }));
            }

            if (this.state.newPassword != text) {
                this.setState(({
                    errorCheckPassword: notmatchText,
                    checkPassword: text,
                }));
            }
        }
    }

    verifyForm = () => {
        let formOk = true;

        if (this.state.newPassword == "") {
            this.setState(({
                errorCheckPassword: invalidText,
            }));
            formOk = false;
        }

        if (this.state.checkPassword == "") {
            this.setState(({
                errorCheckPassword: invalidText,
            }));
            formOk = false;
        }

        if (this.state.newPassword != this.state.checkPassword) {
            this.setState(({
                errorCheckPassword: notmatchText,
            }));
            formOk = false;
        }

        if (this.state.oldPassword == "") {
            this.setState(({
                errorOldPassword: invalidText,
            }));
            formOk = false;
        }

        if (formOk) {
            this.setState(({ disableButton: true }));
            // Submit to server
            const config = {
                headers: { Authorization: `Bearer ${this.state.token}` }
            };
            axios.post
                (
                    `${apiUrl}/changePassword`,
                    {
                        "username": this.state.username,
                        "oldPassword": this.state.oldPassword,
                        "newPassword": this.state.newPassword
                    },
                    config
                )
                .then((response) => {
                    if (response.status == 200) {
                        console.log(response.data);
                        if (response.data.errorCode == 1) {
                            this.setState(
                                (
                                    {
                                        formMessage: response.data.error
                                    }
                                )
                            );
                        } else {
                            this.setState(
                                (
                                    {
                                        formMessage: response.data.message
                                    }
                                )
                            );
                        }

                    }
                })
                .catch((error) => {
                    // console.log(error);
                    if (error.response) {
                        if (error.response.status == 401) {
                            // console.log(error.response.status);
                            Alert.alert(
                                `Session หมดอายุ กรุณาล็อกอินใหม่`,
                                `Session expired, Please login`,
                            );
                        }
                        this.props.navigation.reset({ index: 0, routes: [{ name: 'Home' }], });
                    }
                });

            setTimeout(() => {
                this.setState((prevState) => ({ formMessage: "", disableButton: false, keyGen: prevState.keyGen + 1 }));
            }, 1500);
        } else {
            this.setState(
                (
                    {
                        formMessage: "Inputs are required"
                    }
                )
            );
            setTimeout(() => {
                this.setState(({ formMessage: "", disableButton: false }));
            }, 1500);
        }
    }

    render() {
        return (
            <View style={styles.container} key={this.state.keyGen}>
                <HeaderMain
                    username={this.state.username}
                    navigation={this.props.navigation}
                    toTransactionPage={(page) => this.showTransactionPage(page)}
                />

                <View style={{ flex: 1 }}>
                    <View style={{ justifyContent: 'center', alignContent: 'center' }}>
                        <Text style={styles.textTitle}>Change Password</Text>
                    </View>

                    <View style={{ marginLeft: '10%', marginTop: '5%', justifyContent: 'center', alignContent: 'center' }}>
                        <Text style={styles.textLabel}>Old password</Text>
                        <TextInput
                            secureTextEntry={true}
                            onChangeText={(text) => this.checkPassword(text, OLD)}
                            style={styles.input}
                            placeholder="รหัสผ่านเดิม"
                            placeholderTextColor="grey"
                        />
                        <Text style={{ color: 'red', marginLeft: '5%', zIndex: -10 }}>{this.state.errorOldPassword != "" ? this.state.errorOldPassword : ""}</Text>
                    </View>

                    <View style={{ marginLeft: '10%', marginTop: '5%', justifyContent: 'center', alignContent: 'center' }}>
                        <Text style={styles.textLabel}>New Password</Text>
                        <TextInput
                            secureTextEntry={true}
                            onChangeText={(text) => this.checkPassword(text, NEW)}
                            style={styles.input}
                            placeholder="รหัสผ่านใหม่"
                            placeholderTextColor="grey"
                        />
                        <Text style={{ color: 'red', marginLeft: '5%', zIndex: -10 }}>{this.state.errorNewPassword != "" ? this.state.errorNewPassword : ""}</Text>
                    </View>

                    <View style={{ marginLeft: '10%', marginTop: '5%', justifyContent: 'center', alignContent: 'center' }}>
                        <Text style={styles.textLabel}>Confirm new password</Text>
                        <TextInput
                            secureTextEntry={true}
                            onChangeText={(text) => this.checkPassword(text, CONFIRM)}
                            style={styles.input}
                            placeholder="ยืนยันรหัสผ่านใหม่"
                            placeholderTextColor="grey"
                        />
                        <Text style={{ color: 'red', marginLeft: '5%', zIndex: -10 }}>{this.state.errorCheckPassword != "" ? this.state.errorCheckPassword : ""}</Text>
                    </View>

                    <View style={this.state.formMessage != "" ? styles.formMessage : { opacity: 0 }}>
                        <Text style={{ color: 'black', zIndex: -10, alignSelf: 'center' }}>{this.state.formMessage != "" ? this.state.formMessage : ""}</Text>
                    </View>

                    <LinearGradient style={this.state.disableButton ? styles.submitButtonDisable : styles.submitButton}
                        colors={['#569fbd', '#65a5bf']}
                        start={[1, 0]}
                        end={[1, 1]}
                    >
                        <TouchableOpacity disabled={this.state.disableButton ? true : false} onPress={() => this.verifyForm()}>
                            <Text style={this.state.disableButton ? styles.textButtonDisable : styles.textButton}>Submit</Text>
                        </TouchableOpacity>
                    </LinearGradient>


                </View>


            </View>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: .9
    },
    formMessage: {
        marginTop: '5%', justifyContent: 'center', alignContent: 'center', alignSelf: 'center',
        borderRadius: 50, width: '60%', height: 60,
        backgroundColor: 'white',
        opacity: .9
    },
    textTitle: {
        fontFamily: 'sukhumvitset-thin-webfont',
        fontSize: 20,
        color: 'white',
        textAlign: 'center',
    },
    textLabel: {
        fontFamily: 'sukhumvitset-thin-webfont',
        fontSize: 16,
        color: 'white',
        textAlign: 'left',
    },
    textButton: {
        fontFamily: 'sukhumvitset-bold-webfont',
        fontSize: 18,
        color: 'white',
        textAlign: 'center',
    },
    textButtonDisable: {
        fontFamily: 'sukhumvitset-bold-webfont',
        fontSize: 18,
        color: 'grey',
        textAlign: 'center',
    },
    input: {
        // height: 40,
        marginTop: 10,
        width: '85%',
        borderBottomWidth: 0.5,
        color: 'white',
        borderColor: 'white',
        justifyContent: 'center',
        paddingHorizontal: 10,
    },
    submitButton: {
        width: '65%',
        height: '8%',
        marginTop: '10%',
        borderWidth: 1,
        borderRadius: 5,
        justifyContent: 'center',
        alignContent: 'center',
        alignSelf: 'center'
    },
    submitButtonDisable: {
        opacity: .9,
        backgroundColor: 'grey',
        width: '65%',
        height: '8%',
        marginTop: '10%',
        borderWidth: 1,
        borderRadius: 5,
        justifyContent: 'center',
        alignContent: 'center',
        alignSelf: 'center'
    },
    forgotPassword: {
        color: 'white',
        opacity: 0.5,
        textDecorationLine: 'underline',
        alignSelf: 'flex-end'
    },
    errorText: {
        marginLeft: '10%',
        fontSize: 12,
        color: '#FF0D10',
    },
});



