import { StatusBar } from 'expo-status-bar';
import React, { Component, useEffect } from 'react';
import { Text, StyleSheet, View, ImageBackground } from 'react-native';
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import * as Linking from 'expo-linking';

import * as Font from 'expo-font';

import Header from './Header';
import {
  Home, PhoneRegister, Chat, Main, PlayAccess, Transaction,
  ChangePassword, Promotion, Announce
} from './screens/Index';

const Stack = createStackNavigator();

const CustomFonts = {
  'SanamDeklen_chaya': require('./assets/fonts/SanamDeklen_chaya.ttf'),
  'sukhumvitset-thin-webfont': require('./assets/fonts/PSL162AD.ttf'),
  'sukhumvitset-bold-webfont': require('./assets/fonts/sukhumvitset-bold-webfont.ttf'),
  'sukhumvitset-medium-webfont': require('./assets/fonts/sukhumvitset-medium-webfont.ttf'),
  'sukhumvitset-text-webfont': require('./assets/fonts/sukhumvitset-text-webfont.ttf'),
  'sukhumvitset-thin-webfont': require('./assets/fonts/sukhumvitset-thin-webfont.ttf')
};

const prefix = Linking.createURL('/');

const linking = {
  prefixes: [prefix],
  config: {
    screens: {
      PhoneRegister: {
        path: 'PhoneRegister/:id',
        parse: {
          id: (id) => id.replace(/^id=/, ''),
        },
        stringify: {
          id: (id) => id.replace(/^id=/, ''),
        },
      }
    }
  }
}

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showHeader: true,
      fontsLoaded: false,
      data: [],
    }
  }

  handleDeeplink = (event) => {
    let data = Linking.parse(event.url)
    this.setState({ data: data })
  }

  componentDidMount() {
    this._loadFontsAsync();
  }

  _loadFontsAsync = async () => {
    await Font.loadAsync(CustomFonts);
    this.setState({ fontsLoaded: true });
    console.log("Font loaded..")
  }

  render() {
    let rootView = {};

    rootView = (
      <View style={appStyle.container}>
        <StatusBar style="light" />
        <ImageBackground source={BG_IMAGE} style={{ width: '100%', height: '100.1%', zIndex: -150, }}>

          <NavigationContainer linking={linking} fallback={<Text>Loading...</Text>}>
            <Stack.Navigator
              screenOptions={{
                headerShown: false,
                cardStyle: { backgroundColor: 'transparent' },
              }}>

              <Stack.Screen
                name="Home"
                component={Home}
                options={{
                  navigationOptions: {
                    title: 'Home',
                    headerLeft: null
                  }, headerTitle: props => <Header {...props} />, headerShown: true, headerMode: 'screen', title: 'HOME', headerTransparent: true, backgroundColor: 'transparent'
                }}
              />


              <Stack.Screen
                name="ChangePassword"
                component={ChangePassword}
                headerMode="screen"
                options={{
                  title: 'Change Password'
                }}
              />

              <Stack.Screen
                name="PhoneRegister"
                component={PhoneRegister}
                headerMode="screen"
                options={{
                  headerStyle: {
                    backgroundColor: 'white',
                  },
                  headerTitle: props => <Header Register={true} {...props} />,
                  headerShown: true, title: 'หน้าหลัก', headerTransparent: true, backgroundColor: 'transparent', headerTintColor: 'white'
                }}
              />

              <Stack.Screen
                name="Main"
                component={Main}
                initialParams={{ itemId: 43 }}
                options={{ title: 'Main' }}
              />

              <Stack.Screen
                name="PlayAccess"
                component={PlayAccess}
                options={{ title: 'Games' }}
              />

              <Stack.Screen
                name="Transaction"
                component={Transaction}
                options={{ title: 'Transaction' }}
              />

              <Stack.Screen
                name="Promotion"
                component={Promotion}
                options={{ title: 'Promotion' }}
              />

              <Stack.Screen
                name="Announce"
                component={Announce}
                options={{ title: 'Announce' }}
              />

              <Stack.Screen
                name="Chat"
                component={Chat}
                options={{ title: 'Live Chat', headerShown: false, headerTransparent: true, backgroundColor: 'transparent' }}
              />

            </Stack.Navigator>
          </NavigationContainer>
        </ImageBackground>

      </View>
    )
    return rootView;
  }
};

const BG_IMAGE = require("./assets/img/login_bg.png")

const appStyle = StyleSheet.create({
  container: {
    flex: 1
  }
})

const MainStyle = StyleSheet.create({
  mainContainer: {
    flex: 1,
    // borderWidth: 2,
    borderColor: 'white',
  },
  container: {
    flex: .9,
    marginTop: 45,
    alignItems: 'center',
    // borderWidth: 5,
    borderColor: 'green',
  },
  footerBackground: {
    position: 'absolute',
    bottom: 0,
    borderColor: '#426177',
    borderWidth: 1,
    width: '100%',
    height: 65,
    zIndex: -10,
  },
  footerMainButtonImage: {
    width: '100%',
    alignSelf: "center",
    justifyContent: 'center',
    height: '100%'
  },
  footerButtonImage: {
    width: 30,
    alignSelf: "center",
    justifyContent: 'center',
    // borderWidth: 1,
    height: 30
  },
  footer: {
    flex: .15,
    flexDirection: 'row',
    // flexWrap: 'wrap',
    justifyContent: 'space-between',
    alignContent: 'center',
    // borderWidth: 2,
    borderColor: 'white',
  },
  footerButton: {
    height: 70,
    width: '18%',
    borderRadius: 15,
    flexDirection: 'column',
    alignSelf: 'flex-end',
    alignContent: 'center',
    justifyContent: 'center',
    // borderWidth: 1,
    borderColor: 'white',
  },
  footerIcon: {
    fontSize: 10,
    color: 'white',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  footerMain: {
    height: 90,
    width: '22%',
    borderRadius: 360,
    marginTop: 10,
    alignContent: 'center',
    justifyContent: 'center',
    borderColor: 'white',
    zIndex: 1,
  },
  title: {
    margin: 12,
    alignSelf: 'flex-start',
    fontSize: 15,
    color: 'white',

  },
})

export default App;

